using Unity.Burst;
using Unity.Burst.Intrinsics;
using Unity.Mathematics;
using static Unity.Burst.Intrinsics.X86.Sse;

namespace DotsLibrary
{
	[BurstCompile]
	public static unsafe class SIMDMath
	{
		[BurstCompile(FloatMode = FloatMode.Fast, CompileSynchronously = true)]
		public static void MatrixVectorMultiplyDefault([NoAlias] float4x4* matrixPtr, [NoAlias] float4* vectors, int numVectors)
		{
			for(int i = 0; i < numVectors; i++)
			{
				vectors[i] = math.mul(matrixPtr[i], vectors[i]);
			}
		}
		
		[BurstCompile(FloatMode = FloatMode.Fast, CompileSynchronously = true)]
		public static void MatrixVectorMultiplySIMD([NoAlias] float4x4* matrixPtr, [NoAlias] float4* vectors, int numVectors)
		{
			for(int i = 0; i < numVectors; i++)
			{
				var vector = vectors[i];
				var matrix = matrixPtr[i];
				var x = new v128(vector.x);
				var y = new v128(vector.y);
				var z = new v128(vector.z);
				var w = new v128(vector.w);
				var c0 = load_ps(&matrix.c0);
				var c1 = load_ps(&matrix.c1);
				var c2 = load_ps(&matrix.c2);
				var c3 = load_ps(&matrix.c3);
				var m0 = mul_ps(c0, x);
				var m1 = mul_ps(c1, y);
				var m2 = mul_ps(c2, z);
				var m3 = mul_ps(c3, w);
				var a1 = add_ps(m0, m1);
				var a2 = add_ps(m2, m3);
				var result = add_ps(a1, a2);
				store_ps(&vectors[i], result);
			}
		}
		
		[BurstCompile(FloatMode = FloatMode.Fast, CompileSynchronously = true)]
		public static void RunningSumDefault([NoAlias] int* arr, int length)
		{
			int sum = 0;
			for (int i = 0; i < length; i++)
			{
				sum += arr[i];
				arr[i] = sum;
			}
		}
		
		/// <summary>
		/// Compute a running sum and write it back to the input array, so the i-th entry contains the
		/// sum of all elements up-to and including itself.
		/// </summary>
		[BurstCompile(FloatMode = FloatMode.Fast, CompileSynchronously = true)]
		public static void RunningSumSIMD([NoAlias] int* arr, int length)
		{
			var baseSum = new v128(0, 0, 0, 0);
			
			for(int i = 0; i < length; i += 4)
			{
				var a = arr[i];
				var b = arr[i + 1];
				var c = arr[i + 2];
				var d = arr[i + 3];
				var s0 = new v128(0, 0, 0, a);
				var s1 = new v128(0, 0, a, b);
				var s2 = new v128(0, a, b, c);
				var s3 = new v128(a, b, c, d);
				var a0 = add_ps(s0, s1);
				var a1 = add_ps(s2, s3);
				var f0 = add_ps(a0, a1);
				var f1 = add_ps(f0, baseSum);
				store_ps(&arr[i], f1);
				baseSum = new v128(f1.SInt3);
			}
		}
	}

}

