using DotsLibrary.Collections;
using NUnit.Framework;
using Unity.Collections;
using Unity.PerformanceTesting;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace DotsLibrary
{


	public static unsafe class SphereCollisionBenchmarks
	{
		const int SphereCount = 100_000;
		const uint Seed = 132438297;
		const int WarmupCount = 10;
		const int IterationCount = 10;
		const int MeasurementCount = 10;

		[Test, Performance]
		public static void SphereCollisionSIMDv256Benchmark()
		{
			Random random;
			NativeArray<float> sphereXs = default;
			NativeArray<float> sphereYs = default;
			NativeArray<float> sphereZs = default;
			NativeArray<float> sphereRadii = default;
			NativeArray<Sphere> testSphere = default;

			Measure.Method(
				       () =>
				       {
					       SphereCollisions.SphereCollisionSIMD_v256(
						       sphereXs.GetTypedPtr(),
						       sphereYs.GetTypedPtr(),
						       sphereZs.GetTypedPtr(),
						       sphereRadii.GetTypedPtr(),
						       testSphere.GetTypedPtr(),
						       SphereCount,
						       out var intersectionCount);

					       Debug.Log($"SphereCollisionSIMDv256 IntersectionCount: {intersectionCount}");
				       })
			       .WarmupCount(WarmupCount)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(MeasurementCount)
			       .SetUp(() =>
			       {
				       random = new Random(Seed);
				       sphereXs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereYs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereZs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereRadii = new NativeArray<float>(SphereCount, Allocator.Temp);

				       for(int i = 0; i < SphereCount; i++)
				       {
					       var position = random.NextFloat3();
					       var radius = random.NextFloat();

					       sphereXs[i] = position.x;
					       sphereYs[i] = position.y;
					       sphereZs[i] = position.z;
					       sphereRadii[i] = radius;
				       }

				       testSphere = new NativeArray<Sphere>(1, Allocator.Temp);
				       testSphere[0] = new Sphere
				       {
					       Position = random.NextFloat3(),
					       Radius = random.NextFloat(),
				       };
			       })
			       .Run();
		}

		[Test, Performance]
		public static void SphereCollisionSIMDv128Benchmark()
		{
			Random random;
			NativeArray<float> sphereXs = default;
			NativeArray<float> sphereYs = default;
			NativeArray<float> sphereZs = default;
			NativeArray<float> sphereRadii = default;
			NativeArray<Sphere> testSphere = default;

			Measure.Method(
				       () =>
				       {
					       SphereCollisions.SphereCollisionSIMD_v128(
						       sphereXs.GetTypedPtr(),
						       sphereYs.GetTypedPtr(),
						       sphereZs.GetTypedPtr(),
						       sphereRadii.GetTypedPtr(),
						       testSphere.GetTypedPtr(),
						       SphereCount,
						       out var intersectionCount);

					       Debug.Log($"SphereCollisionSIMDv128 IntersectionCount: {intersectionCount}");
				       })
			       .WarmupCount(WarmupCount)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(MeasurementCount)
			       .SetUp(() =>
			       {
				       random = new Random(Seed);
				       sphereXs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereYs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereZs = new NativeArray<float>(SphereCount, Allocator.Temp);
				       sphereRadii = new NativeArray<float>(SphereCount, Allocator.Temp);

				       for(int i = 0; i < SphereCount; i++)
				       {
					       var position = random.NextFloat3();
					       var radius = random.NextFloat();

					       sphereXs[i] = position.x;
					       sphereYs[i] = position.y;
					       sphereZs[i] = position.z;
					       sphereRadii[i] = radius;
				       }

				       testSphere = new NativeArray<Sphere>(1, Allocator.Temp);
				       testSphere[0] = new Sphere
				       {
					       Position = random.NextFloat3(),
					       Radius = random.NextFloat(),
				       };
			       })
			       .Run();
		}

		[Test, Performance]
		public static void SphereCollisionDefaultBenchmark()
		{
			Random random;
			NativeArray<Sphere> spheres = default;
			NativeArray<Sphere> testSphere = default;

			Measure.Method(
				       () =>
				       {
					       SphereCollisions.SphereCollisionDefault(
						       testSphere.GetTypedPtr(),
						       spheres.GetTypedPtr(),
						       SphereCount,
						       out var intersectionCount);

					       Debug.Log($"SphereCollisionDefault IntersectionCount: {intersectionCount}");
				       })
			       .WarmupCount(WarmupCount)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(MeasurementCount)
			       .SetUp(() =>
			       {
				       random = new Random(Seed);
				       spheres = new NativeArray<Sphere>(SphereCount, Allocator.Temp);
				       for(int i = 0; i < spheres.Length; i++)
				       {
					       spheres[i] = new Sphere
					       {
						       Position = random.NextFloat3(),
						       Radius = random.NextFloat(),
					       };
				       }

				       testSphere = new NativeArray<Sphere>(1, Allocator.Temp);
				       testSphere[0] = new Sphere
				       {
					       Position = random.NextFloat3(),
					       Radius = random.NextFloat(),
				       };
			       })
			       .Run();
		}

		[Test, Performance]
		public static void SphereCollisionNoBranchBenchmark()
		{
			Random random;
			NativeArray<Sphere> spheres = default;
			NativeArray<Sphere> testSphere = default;

			Measure.Method(
				       () =>
				       {
					       SphereCollisions.SphereCollisionNoBranch(
						       testSphere.GetTypedPtr(),
						       spheres.GetTypedPtr(),
						       SphereCount,
						       out var intersectionCount);

					       Debug.Log($"SphereCollisionNoBranch IntersectionCount: {intersectionCount}");
				       })
			       .WarmupCount(WarmupCount)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(MeasurementCount)
			       .SetUp(() =>
			       {
				       random = new Random(Seed);
				       spheres = new NativeArray<Sphere>(SphereCount, Allocator.Temp);
				       for(int i = 0; i < spheres.Length; i++)
				       {
					       spheres[i] = new Sphere
					       {
						       Position = random.NextFloat3(),
						       Radius = random.NextFloat(),
					       };
				       }

				       testSphere = new NativeArray<Sphere>(1, Allocator.Temp);
				       testSphere[0] = new Sphere
				       {
					       Position = random.NextFloat3(),
					       Radius = random.NextFloat(),
				       };
			       })
			       .Run();
		}
	}
}