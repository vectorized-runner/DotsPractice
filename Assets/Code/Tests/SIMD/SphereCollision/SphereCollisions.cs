using System;
using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Burst.Intrinsics;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Burst.Intrinsics.X86.Sse;
using static Unity.Burst.Intrinsics.X86.Avx;

namespace DotsLibrary
{
	public struct Sphere
	{
		public float3 Position;
		public float Radius;
	}

	// Task: Given a list of spheres and another sphere, compute the index of the first sphere that overlaps with the additional
	// sphere, and the number of intersections in total.
	[BurstCompile]
	public static unsafe class SphereCollisions
	{
		[BurstCompile]
		public static void SphereCollisionSIMD_v256([NoAlias] float* sphereXs, [NoAlias] float* sphereYs,
		                                            [NoAlias] float* sphereZs, [NoAlias] float* sphereRadii,
		                                            [NoAlias] Sphere* testSphere, int sphereCount,
		                                            out int intersectionCount)
		{
			const int v256Size = 8;

			if(sphereCount % v256Size != 0)
				throw new NotImplementedException($"SphereCount needs to be a multiple of {v256Size}.");

			intersectionCount = 0;
			
			for(int i = 0; i < sphereCount; i += v256Size)
			{
				var xs = mm256_load_ps(sphereXs + i);
				var ys = mm256_load_ps(sphereYs + i);
				var zs = mm256_load_ps(sphereZs + i);

				var xDiffs = mm256_sub_ps(xs, new v256(testSphere->Position.x));
				var yDiffs = mm256_sub_ps(ys, new v256(testSphere->Position.y));
				var zDiffs = mm256_sub_ps(zs, new v256(testSphere->Position.z));

				var xDiffSq = mm256_mul_ps(xDiffs, xDiffs);
				var yDiffSq = mm256_mul_ps(yDiffs, yDiffs);
				var zDiffSq = mm256_mul_ps(zDiffs, zDiffs);

				var distSq = mm256_add_ps(xDiffSq, mm256_add_ps(yDiffSq, zDiffSq));

				var radii = mm256_load_ps(sphereRadii + i);
				var radiiSum = mm256_add_ps(radii, new v256(testSphere->Radius));
				var radiiSq = mm256_mul_ps(radiiSum, radiiSum);

				var collisionMask = new v256(cmple_ps(distSq.Lo128, radiiSq.Lo128), cmple_ps(distSq.Hi128, radiiSq.Hi128));
				intersectionCount += GetNonZeroCount(collisionMask);
			}
		}
		
		[BurstCompile]
		public static void SphereCollisionSIMD_v128([NoAlias] float* sphereXs, [NoAlias] float* sphereYs,
		                                            [NoAlias] float* sphereZs, [NoAlias] float* sphereRadii,
		                                            [NoAlias] Sphere* testSphere, int sphereCount,
		                                            out int intersectionCount)
		{
			const int v128Size = 4;

			if(sphereCount % v128Size != 0)
				throw new NotImplementedException($"SphereCount needs to be a multiple of {v128Size}.");

			intersectionCount = 0;

			for(int i = 0; i < sphereCount; i += v128Size)
			{
				var xs = load_ps(sphereXs + i);
				var ys = load_ps(sphereYs + i);
				var zs = load_ps(sphereZs + i);

				var xDiffs = sub_ps(xs, new v128(testSphere->Position.x));
				var yDiffs = sub_ps(ys, new v128(testSphere->Position.y));
				var zDiffs = sub_ps(zs, new v128(testSphere->Position.z));

				var xDiffSq = mul_ps(xDiffs, xDiffs);
				var yDiffSq = mul_ps(yDiffs, yDiffs);
				var zDiffSq = mul_ps(zDiffs, zDiffs);

				var distSq = add_ps(xDiffSq, add_ps(yDiffSq, zDiffSq));

				var radii = load_ps(sphereRadii + i);
				var radiiSum = add_ps(radii, new v128(testSphere->Radius));
				var radiiSq = mul_ps(radiiSum, radiiSum);

				// If distance squared is less than radius squared, then collide
				var collisionMask = cmple_ps(distSq, radiiSq);
				intersectionCount += GetNonZeroCount(collisionMask);
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static int BoolToInt(bool value)
		{
			return *(byte*)&value;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static int GetNonZeroCount(v256 v)
		{
			return
				BoolToInt(v.SInt0 != 0) +
				BoolToInt(v.SInt1 != 0) +
				BoolToInt(v.SInt2 != 0) +
				BoolToInt(v.SInt3 != 0) +
				BoolToInt(v.SInt4 != 0) +
				BoolToInt(v.SInt5 != 0) +
				BoolToInt(v.SInt6 != 0) +
				BoolToInt(v.SInt7 != 0);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static int GetNonZeroCount(v128 v)
		{
			return
				BoolToInt(v.SInt0 != 0) +
				BoolToInt(v.SInt1 != 0) +
				BoolToInt(v.SInt2 != 0) +
				BoolToInt(v.SInt3 != 0);
		}
		

		[BurstCompile]
		public static void SphereCollisionNoBranch([NoAlias] Sphere* sphereToTest, [NoAlias] Sphere* spheres,
		                                           int sphereCount, out int intersectionCount)
		{
			intersectionCount = 0;
			var spherePositionTest = sphereToTest->Position;
			var sphereRadiusTest = sphereToTest->Radius;

			for(int i = 0; i < sphereCount; i++)
			{
				// Unity.Burst.CompilerServices.Loop.ExpectNotVectorized();
				
				var spherePositionA = spheres[i].Position;
				var sphereRadiusA = spheres[i].Radius;
				var distanceSq = math.distancesq(spherePositionA, spherePositionTest);
				var radius = sphereRadiusA + sphereRadiusTest;
				var radiusSq = radius * radius;
				var inRadius = distanceSq < radiusSq;
				intersectionCount += BoolToInt(inRadius);
			}
		}

		[BurstCompile]
		public static void SphereCollisionDefault([NoAlias] Sphere* sphereToTest, [NoAlias] Sphere* spheres,
		                                          int sphereCount, out int intersectionCount)
		{
			intersectionCount = 0;

			var spherePositionTest = sphereToTest->Position;
			var sphereRadiusTest = sphereToTest->Radius;

			for(int i = 0; i < sphereCount; i++)
			{
				Unity.Burst.CompilerServices.Loop.ExpectNotVectorized();
				
				var spherePositionA = spheres[i].Position;
				var sphereRadiusA = spheres[i].Radius;
				var distanceSq = math.distancesq(spherePositionA, spherePositionTest);
				var radius = sphereRadiusA + sphereRadiusTest;
				var radiusSq = radius * radius;

				if(distanceSq < radiusSq)
				{
					intersectionCount++;
				}
			}
		}

		[BurstDiscard]
		static void v128LogInts(v128 v)
		{
			Debug.Log($"{v.SInt0}, {v.SInt1}, {v.SInt2}, {v.SInt3}");
		}

		[BurstDiscard]
		static void v128LogBinaryString(v128 v)
		{
			Debug.Log(Convert.ToString(v.SInt0, 2));
			Debug.Log(Convert.ToString(v.SInt1, 2));
			Debug.Log(Convert.ToString(v.SInt2, 2));
			Debug.Log(Convert.ToString(v.SInt3, 2));
		}
	}
}