using System;
using NUnit.Framework;
using UnityEngine;

namespace DotsLibrary
{
	public static class ToStringNoAllocTests
	{
		[TestCase(int.MinValue)]
		[TestCase(-2424870)]
		[TestCase(-312489)]
		[TestCase(-2)]
		[TestCase(-1)]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		[TestCase(5)]
		[TestCase(10)]
		[TestCase(100)]
		[TestCase(214)]
		[TestCase(4512)]
		[TestCase(29430)]
		[TestCase(348258)]
		[TestCase(2439583)]
		[TestCase(239423948)]
		[TestCase(int.MaxValue)]
		public static void IntToString(int value)
		{
			var result = value.ToStringNoAlloc(new char[12]);
			Assert.IsTrue(value.ToString().Equals(result.ToString(), StringComparison.Ordinal));
		}

		[TestCase(int.MinValue)]
		[TestCase(-2424870)]
		[TestCase(-312489)]
		[TestCase(-2)]
		[TestCase(-1)]
		[TestCase(0)]
		[TestCase(1)]
		[TestCase(2)]
		[TestCase(5)]
		[TestCase(10)]
		[TestCase(100)]
		[TestCase(214)]
		[TestCase(4512)]
		[TestCase(29430)]
		[TestCase(348258)]
		[TestCase(2439583)]
		[TestCase(239423948)]
		[TestCase(int.MaxValue)]
		public static void BurstIntToString(int value)
		{
			var result = value.ToStringNoAllocBurst(new char[12]);
			Assert.IsTrue(value.ToString().Equals(result.ToString(), StringComparison.Ordinal));
		}
		
		[Test]
		public static void BurstIntToStringTest()
		{
			var result = IntExtensions.ToStringNoAllocBurst(-123456, new char[12]);
			
			Debug.Log(result.Length);
			Debug.Log(result.ToString());

			Assert.IsTrue(result.ToString().Equals(result.ToString(), StringComparison.Ordinal));
		}
		
		[Test]
		public static void WorksWithBufferLength11()
		{
			var result = int.MinValue.ToStringNoAlloc(new char[11]);
			Assert.IsTrue(result.Equals(result.ToString(), StringComparison.Ordinal));
		}
		
		[Test]
		public static void DoesNotWithBufferLengthLessThan11()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				var result = int.MinValue.ToStringNoAlloc(new char[10]);
			});
		}

	}
}