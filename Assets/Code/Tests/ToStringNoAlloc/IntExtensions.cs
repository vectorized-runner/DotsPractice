using System;
using System.Runtime.CompilerServices;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;

namespace DotsLibrary
{
	[BurstCompile]
	public static unsafe partial class IntExtensions
	{
		static readonly char[] Digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		const string IntMinString = "-2147483648";

		public static ReadOnlySpan<char> ToStringNoAllocBurst(this int value, Span<char> buffer12Bytes)
		{
			CheckBufferLength(buffer12Bytes);

			fixed(void* intMinPtr = IntMinString)
			fixed(void* digitsPtr = Digits)
			fixed(void* bufferPtr = buffer12Bytes)
			{
				ToStringNoAllocBurst_Impl(value, bufferPtr, digitsPtr, intMinPtr, buffer12Bytes.Length, out var resultStart, out var resultLength);
				var charPtr = (char*)bufferPtr + resultStart;
				return new ReadOnlySpan<char>(charPtr, resultLength);
			}
		}

		[BurstCompile]
		public static void ToStringNoAllocBurst_Impl(this int value, void* buffer, void* digitsPtr, void* intMinPtr, int bufferLength, out int resultStart,
		                                          out int resultLength)
		{
			const int baseTen = 10;
			var charBuffer = (char*)buffer;

			switch(value)
			{
				case 0:
				{
					charBuffer[0] = '0';
					resultStart = 0;
					resultLength = 1;
					return;
				}
				case int.MinValue:
				{
					// You can't use managed string here, it crashes
					UnsafeUtility.MemCpy(buffer, intMinPtr, 11 * UnsafeUtility.SizeOf<char>());
					resultStart = 0;
					resultLength = 11;
					return;
				}
				case > 0:
				{
					int i = 11;
					while(value > 0)
					{
						charBuffer[i] = UnsafeUtility.ReadArrayElement<char>(digitsPtr, value % baseTen);
						i--;
						value /= baseTen;
					}

					resultStart = i + 1;
					resultLength = bufferLength - 1 - i;
					return;
				}
				case < 0:
				{
					int i = 11;
					value = Math.Abs(value);

					while(value > 0)
					{
						charBuffer[i] = UnsafeUtility.ReadArrayElement<char>(digitsPtr, value % baseTen);
						i--;
						value /= baseTen;
					}

					charBuffer[i] = '-';
					resultStart = i;
					resultLength = bufferLength - i;
					return;
				}
			}
		}

		public static ReadOnlySpan<char> ToStringNoAlloc(this int value, Span<char> buffer12Chars)
		{
			CheckBufferLength(buffer12Chars);

			const int baseTen = 10;

			switch(value)
			{
				case 0:
				{
					buffer12Chars[0] = '0';
					return buffer12Chars[..1];
				}
				case int.MinValue:
				{
					// Very special case, you can't take Abs of int.MinValue
					IntMinString.AsSpan().CopyTo(buffer12Chars);
					return buffer12Chars[..11];
				}
				case > 0:
				{
					int i = 11;
					while(value > 0)
					{
						buffer12Chars[i] = Digits[value % baseTen];
						i--;
						value /= baseTen;
					}

					return buffer12Chars[(i + 1)..];
				}
				case < 0:
				{
					int i = 11;
					value = Math.Abs(value);

					while(value > 0)
					{
						buffer12Chars[i] = Digits[value % baseTen];
						i--;
						value /= baseTen;
					}

					buffer12Chars[i] = '-';
					return buffer12Chars[i..];
				}
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static void CheckBufferLength(Span<char> buffer)
		{
			// ToString can be at most 11 characters, for int.MinValue, but 12 bytes is better since it's even number?
			if(buffer.Length < 11)
				throw new InvalidOperationException("Buffer length must be at least length 11 for safe conversion.");
		}
	}
}