using System.Runtime.CompilerServices;
using NUnit.Framework;
using Unity.PerformanceTesting;
using UnityEngine;

namespace DotsLibrary
{
	public static class StructReturnBenchmarks
	{
		public readonly struct TheStruct
		{
			public readonly Matrix4x4 Value;

			public TheStruct(Matrix4x4 value)
			{
				Value = value;
			}

			public Matrix4x4 GetValue()
			{
				return Value;
			}
		}

		public static Matrix4x4 GetIdentity => Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Matrix4x4 GetIdentityInlined()
		{
			return Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
		}
		
		public class TheClass
		{
			public readonly Matrix4x4 Value;

			public TheClass(Matrix4x4 value)
			{
				Value = value;
			}

			public ref readonly Matrix4x4 RefValue => ref Value;
		}

		[Test, Performance]
		public static void CreateLocalBenchmark()
		{
			Measure.Method(() =>
			       {
				       var lhs = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}
		
		[Test, Performance]
		public static void GetFromMethodBenchmark()
		{
			Measure.Method(() =>
			       {
				       var lhs = GetIdentityInlined();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}
		
		[Test, Performance]
		public static void GetFromClassBenchmark()
		{
			var theClass = new TheClass(GetIdentity);
			
			Measure.Method(() =>
			       {
				       var lhs = theClass.Value;
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}
		
		[Test, Performance]
		public static void GetFromClassAsRefReadOnlyBenchmark()
		{
			var theClass = new TheClass(GetIdentity);
			
			Measure.Method(() =>
			       {
				       var lhs = theClass.RefValue;
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}
		
		[Test, Performance]
		public static void GetFromClassAsRefReadOnlyBenchmark_2()
		{
			var theClass = new TheClass(GetIdentity);
			
			Measure.Method(() =>
			       {
				       ref readonly var lhs = ref theClass.RefValue;
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}

		[Test, Performance]
		public static void GetFromStructBenchmark()
		{
			var theStruct = new TheStruct(GetIdentity);
			
			Measure.Method(() =>
			       {
				       var lhs = theStruct.Value;
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}

		[Test, Performance]
		public static void GetFromStructPropertyBenchmark()
		{
			var theStruct = new TheStruct(GetIdentity);
			
			Measure.Method(() =>
			       {
				       var lhs = theStruct.GetValue();
			       })
			       .WarmupCount(10)
			       .MeasurementCount(10)
			       .IterationsPerMeasurement(10000)
			       .Run();
		}
	}
}


