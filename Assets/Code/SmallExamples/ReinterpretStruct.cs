using System;
using NUnit.Framework;
using Unity.Mathematics;

namespace DotsLibrary
{
	public static unsafe class ReinterpretStruct
	{
		// You can use Unsafe.As instead of this, and pay zero performance cost!
		public static T2 Reinterpret<T1, T2>(this T1 unmanagedType) where T1 : unmanaged where T2 : unmanaged
		{
			if(sizeof(T1) != sizeof(T2))
				throw new InvalidOperationException();
			return *(T2*)(&unmanagedType);
		}

		public struct StructA
		{
			public int A;
			public int B;
			public int C;
			public int D;
		}

		public struct StructB
		{
			public int4 ABCD;
		}

		[Test]
		public static void ReinterpretWorks()
		{
			var structB = new StructB
			{
				ABCD = new int4(1, 2, 3, 4),
			};

			var structA = structB.Reinterpret<StructB, StructA>();

			Assert.AreEqual(1, structA.A);
			Assert.AreEqual(2, structA.B);
			Assert.AreEqual(3, structA.C);
			Assert.AreEqual(4, structA.D);
		}
	}
}