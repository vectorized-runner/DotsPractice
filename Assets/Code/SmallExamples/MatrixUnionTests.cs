using System.Runtime.InteropServices;
using NUnit.Framework;
using Unity.Mathematics;
using UnityEngine;

namespace DotsLibrary
{
	public static unsafe class MatrixUnionTests
	{
		[StructLayout(LayoutKind.Explicit)]
		public struct MatrixUnion
		{
			[FieldOffset(0)]
			public Matrix4x4 Matrix1;

			[FieldOffset(0)]
			public float4x4 Matrix2;
		}

		[Test]
		public static void StructSize_1()
		{
			Assert.IsTrue(sizeof(Matrix4x4) == sizeof(float4x4));
		}

		[Test]
		public static void StructSize_2()
		{
			Assert.IsTrue(sizeof(Matrix4x4) == sizeof(MatrixUnion));
		}
		
		[Test]
		public static void StructAssignment_1()
		{
			var union = new MatrixUnion();
			union.Matrix1 = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
			
			Assert.AreEqual(float4x4.TRS(float3.zero, quaternion.identity, new float3(1, 1, 1)), union.Matrix2);
		}
		
		[Test]
		public static void StructAssignment_2()
		{
			var union = new MatrixUnion();
			union.Matrix2 = float4x4.TRS(float3.zero, quaternion.identity, new float3(1, 1, 1));
			
			Assert.AreEqual(Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one), union.Matrix1);
		}

		[Test]
		public static void PointerConversion()
		{
			var matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
			var unityMatrix = *(float4x4*)&matrix;
			
			Assert.AreEqual(float4x4.TRS(float3.zero, quaternion.identity, new float3(1, 1, 1)), unityMatrix);
		}
	}
}