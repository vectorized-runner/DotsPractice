using System;
using System.Runtime.InteropServices;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;

namespace DotsLibrary
{
	public static class CreateNativeArrayFromManaged
	{
		[BurstCompile]
		public struct DummyNativeArrayJob : IJob
		{
			public NativeArray<int> Array;
			public NativeReference<int> Sum;

			public void Execute()
			{
				for(int i = 0; i < Array.Length; i++)
				{
					Sum.Value += Array[i];
				}
			}
		}

		[Test]
		public static unsafe void ThrowsWhenJobIsExecutedWithoutSafetyHandle()
		{
			var managed = new int[10];
			for(int i = 0; i < managed.Length; i++)
			{
				managed[i] += i;
			}
			
			var handle = GCHandle.Alloc(managed, GCHandleType.Pinned);
			var ptr = handle.AddrOfPinnedObject().ToPointer();
			var nativeArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<int>(ptr, 10, Allocator.Invalid);
			var sum = new NativeReference<int>(Allocator.TempJob);
			
			// Let's try it without safety handle first
			//NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nativeArray, AtomicSafetyHandle.Create());
			
			Assert.Throws<NullReferenceException>(() =>
			{
				new DummyNativeArrayJob
				{
					Array = nativeArray,
					Sum = sum
				}.Execute();

				sum.Dispose();
			});
		}
		
		[Test]
		public static unsafe void DoesNotThrowWhenJobIsExecutedWithSafetyHandle()
		{
			var managed = new int[10];
			for(int i = 0; i < managed.Length; i++)
			{
				managed[i] += i;
			}
			
			var handle = GCHandle.Alloc(managed, GCHandleType.Pinned);
			var ptr = handle.AddrOfPinnedObject().ToPointer();
			var nativeArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<int>(ptr, 10, Allocator.Invalid);
			var sum = new NativeReference<int>(Allocator.TempJob);
			NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nativeArray, AtomicSafetyHandle.Create());
			
			Assert.DoesNotThrow(() =>
			{
				new DummyNativeArrayJob
				{
					Array = nativeArray,
					Sum = sum,
				}.Execute();

				sum.Dispose();
			});
		}
	}
}