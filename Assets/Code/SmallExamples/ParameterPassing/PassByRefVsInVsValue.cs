using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

namespace BurstSIMDPractice
{
	[BurstCompile]
	public struct TranslateMatricesDefaultJob : IJob
	{
		public NativeArray<float4x4> Matrices;
		public float3 Translation;

		public void Execute()
		{
			for(int i = 0; i < Matrices.Length; i++)
			{
				Matrices[i] = MatrixTranslateDefault(Matrices[i], Translation);
			}
		}

		static float4x4 MatrixTranslateDefault(float4x4 matrix, in float3 translation)
		{
			var copy = matrix;
			copy.c3.xyz += translation;

			return copy;
		}
	}

	[BurstCompile]
	public struct TranslateMatricesInJob : IJob
	{
		public NativeArray<float4x4> Matrices;
		public float3 Translation;

		public void Execute()
		{
			for(int i = 0; i < Matrices.Length; i++)
			{
				Matrices[i] = MatrixTranslateIn(Matrices[i], Translation);
			}
		}

		static float4x4 MatrixTranslateIn(in float4x4 matrix, in float3 translation)
		{
			var copy = matrix;
			copy.c3.xyz += translation;

			return copy;
		}
	}
	
	[BurstCompile]
	public unsafe struct TranslateMatricesGetRefPassInJob : IJob
	{
		public NativeArray<float4x4> Matrices;
		public float3 Translation;

		public void Execute()
		{
			for(int i = 0; i < Matrices.Length; i++)
			{
				ref var matrix = ref UnsafeUtility.ArrayElementAsRef<float4x4>(Matrices.GetUnsafePtr(), i);
				Matrices[i] = MatrixTranslateIn(matrix, Translation);
			}
		}

		static float4x4 MatrixTranslateIn(in float4x4 matrix, in float3 translation)
		{
			var copy = matrix;
			copy.c3.xyz += translation;

			return copy;
		}
	}

	[BurstCompile]
	public unsafe struct TranslateMatricesRefJob : IJob
	{
		public NativeArray<float4x4> Matrices;
		public float3 Translation;

		public void Execute()
		{
			for(int i = 0; i < Matrices.Length; i++)
			{
				ref var matrix = ref UnsafeUtility.ArrayElementAsRef<float4x4>(Matrices.GetUnsafePtr(), i);
				MatrixTranslateRef(ref matrix, Translation);
			}
		}

		static void MatrixTranslateRef(ref float4x4 matrix, in float3 translation)
		{
			var position = matrix.c3.xyz + translation;
			matrix.c3.xyz = position;
		}
	}
}