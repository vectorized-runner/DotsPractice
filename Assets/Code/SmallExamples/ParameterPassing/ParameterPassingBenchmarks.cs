using NUnit.Framework;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace BurstSIMDPractice.Tests
{
	public static class ParameterPassingBenchmarks
	{
		[Test, Performance]
		public static void TranslateMatricesDefaultBenchmark()
		{
			Random random = new Random(23489234);
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       new TranslateMatricesDefaultJob
				       {
					       Matrices = matrices,
					       Translation = random.NextFloat3(),
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       matrices = new NativeArray<float4x4>(100_000, Allocator.TempJob);
				       for(int i = 0; i < matrices.Length; i++)
				       {
					       matrices[i] = float4x4.TRS(random.NextFloat3(), quaternion.identity, random.NextFloat3());
				       }
			       })
			       .CleanUp(() =>
			       {
				       matrices.Dispose();
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void TranslateMatricesInBenchmark()
		{
			Random random = new Random(23489234);
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       new TranslateMatricesInJob()
				       {
					       Matrices = matrices,
					       Translation = random.NextFloat3(),
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       matrices = new NativeArray<float4x4>(100_000, Allocator.TempJob);
				       for(int i = 0; i < matrices.Length; i++)
				       {
					       matrices[i] = float4x4.TRS(random.NextFloat3(), quaternion.identity, random.NextFloat3());
				       }
			       })
			       .CleanUp(() =>
			       {
				       matrices.Dispose();
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void TranslateMatricesGetRefPassInBenchmark()
		{
			Random random = new Random(23489234);
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       new TranslateMatricesGetRefPassInJob()
				       {
					       Matrices = matrices,
					       Translation = random.NextFloat3(),
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       matrices = new NativeArray<float4x4>(100_000, Allocator.TempJob);
				       for(int i = 0; i < matrices.Length; i++)
				       {
					       matrices[i] = float4x4.TRS(random.NextFloat3(), quaternion.identity, random.NextFloat3());
				       }
			       })
			       .CleanUp(() =>
			       {
				       matrices.Dispose();
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void TranslateMatricesRefBenchmark()
		{
			Random random = new Random(23489234);
			NativeArray<float4x4> matrices = default;

			Measure.Method(() =>
			       {
				       new TranslateMatricesRefJob
				       {
					       Matrices = matrices,
					       Translation = random.NextFloat3(),
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       matrices = new NativeArray<float4x4>(100_000, Allocator.TempJob);
				       for(int i = 0; i < matrices.Length; i++)
				       {
					       matrices[i] = float4x4.TRS(random.NextFloat3(), quaternion.identity, random.NextFloat3());
				       }
			       })
			       .CleanUp(() =>
			       {
				       matrices.Dispose();
			       })
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .Run();
		}
	}
}