using System.Diagnostics;
using NUnit.Framework;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace DotsLibrary
{
    
	public static class DoubleVsSingleForLoop
	{
		[Test, Performance]
		public static void ForLoopOnce()
		{
			var arr1 = new float[100000];
			var arr2 = new float[100000];
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < arr1.Length; i++)
				       {
					       arr1[i] = random.NextFloat();
					       arr2[i] = random.NextFloat();
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void ForLoopTwice()
		{
			var arr1 = new float[100000];
			var arr2 = new float[100000];
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < arr1.Length; i++)
				       {
					       arr1[i] = random.NextFloat();
				       }
				       for(int i = 0; i < arr2.Length; i++)
				       {
					       arr2[i] = random.NextFloat();
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
	}

}
