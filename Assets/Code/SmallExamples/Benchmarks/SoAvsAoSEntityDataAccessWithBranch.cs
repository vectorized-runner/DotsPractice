using System.Diagnostics;
using NUnit.Framework;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace DotsLibrary
{
	// TODO: Add predictable branch version of this.
	public static class SoAvsAoSEntityDataAccessWithBranch
	{
		public struct EntityPacked
		{
			public float4x4 Matrix;
			public bool IsAlive;
		}

		public struct EntityMatrix
		{
			public float4x4 Value;
		}

		public struct EntityAlive
		{
			public bool Value;
		}
		
		[Test, Performance]
		public static void PackedEntityBenchmark()
		{
			var packedEntities = new EntityPacked[10000];
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < packedEntities.Length; i++)
				       {
					       if(packedEntities[i].IsAlive)
					       {
						       packedEntities[i].Matrix = float4x4.identity;
					       }
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < packedEntities.Length; i++)
				       {
					       packedEntities[i].IsAlive = random.NextBool();
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void NonEntityBenchmark()
		{
			var entityAlives = new EntityAlive[10000];
			var entityMatrices = new EntityMatrix[10000];
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       for(int i = 0; i < entityMatrices.Length; i++)
				       {
					       if(entityAlives[i].Value)
					       {
						       entityMatrices[i].Value = float4x4.identity;
					       }
				       }
			       })
			       .SetUp(() =>
			       {
				       for(int i = 0; i < entityAlives.Length; i++)
				       {
					       entityAlives[i].Value = random.NextBool();
				       }
			       })
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
	}
}