using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace DotsLibrary
{
	// Soo... Idk if sorting first is good or not. It sounds like it should perform better, but it doesn't in benchmarks.
	// I don't know if I'm implementing a bad example though...
	public static class SortItFirst
	{
		public struct ComputeItem
		{
			public int ValueA;
			public int ValueB;
			public bool OtherValue;
		}

		[BurstCompile]
		public struct NotSortedJob : IJob
		{
			public NativeList<ComputeItem> ComputeItems;
			public NativeReference<float> Result;

			public void Execute()
			{
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					if(ComputeItems[i].ValueA > 10)
					{
						Result.Value += ComputeItems[i].ValueA;

						if(ComputeItems[i].ValueB > 15)
						{
							Result.Value += math.sqrt(ComputeItems[i].ValueB);

							if(ComputeItems[i].OtherValue)
							{
								Result.Value++;
							}
						}
					}
				}
			}
		}

		[BurstCompile]
		public struct SortedJob : IJob
		{
			public NativeList<ComputeItem> ComputeItems;
			public NativeReference<float> Result;

			public void Execute()
			{
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					if(ComputeItems[i].ValueA <= 10)
					{
						ComputeItems.RemoveAtSwapBack(i);
						i--;
					}
				}

				for(int i = 0; i < ComputeItems.Length; i++)
				{
					Result.Value += ComputeItems[i].ValueA;
				}
				
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					if(ComputeItems[i].ValueB <= 15)
					{
						ComputeItems.RemoveAtSwapBack(i);
						i--;
					}
				}
				
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					Result.Value += math.sqrt(ComputeItems[i].ValueB);
				}
				
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					if(!ComputeItems[i].OtherValue)
					{
						ComputeItems.RemoveAtSwapBack(i);
						i--;
					}
				}
				
				for(int i = 0; i < ComputeItems.Length; i++)
				{
					Result.Value++;
				}
			}
		}

		[Test, Performance]
		public static void NotSortedBenchmark()
		{
			NativeList<ComputeItem> items = default;
			NativeReference<float> result = default;
			var random = new Random(10);

			Measure.Method(() =>
			       {
				       new NotSortedJob
				       {
					       ComputeItems = items,
					       Result = result
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       items = new NativeList<ComputeItem>(Allocator.TempJob);
				       result = new NativeReference<float>(Allocator.TempJob);

				       for(int i = 0; i < 100000; i++)
				       {
					       items.Add(new ComputeItem
					       {
						       ValueA = random.NextInt(20),
						       ValueB = random.NextInt(20),
						       OtherValue = random.NextBool(),
					       });
				       }
			       })
			       .CleanUp(() =>
			       {
				       items.Dispose();
				       result.Dispose();
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void SortedBenchmark()
		{
			NativeList<ComputeItem> items = default;
			NativeReference<float> result = default;
			var random = new Random(10);

			Measure.Method(() =>
			       {
				       new SortedJob
				       {
					       ComputeItems = items,
					       Result = result
				       }.Run();
			       })
			       .SetUp(() =>
			       {
				       items = new NativeList<ComputeItem>(Allocator.TempJob);
				       result = new NativeReference<float>(Allocator.TempJob);

				       for(int i = 0; i < 100000; i++)
				       {
					       items.Add(new ComputeItem
					       {
						       ValueA = random.NextInt(20),
						       ValueB = random.NextInt(20),
						       OtherValue = random.NextBool(),
					       });
				       }
			       })
			       .CleanUp(() =>
			       {
				       items.Dispose();
				       result.Dispose();
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
	}
}