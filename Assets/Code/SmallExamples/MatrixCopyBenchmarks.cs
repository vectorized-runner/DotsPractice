using System;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.PerformanceTesting;
using UnityEngine;

namespace DotsLibrary
{
	[BurstCompile]
	public static unsafe class MatrixCopyBenchmarks
	{
		public static readonly int MemSize = sizeof(Matrix4x4) * Count;
		public const int Count = 10000;
		public static readonly Matrix4x4[] Source = new Matrix4x4[Count];
		public static readonly Matrix4x4[] Destination = new Matrix4x4[Count];
		public static readonly Matrix4x4 Target = Matrix4x4.TRS(new Vector3(1f, 2f, 3f),
			Quaternion.Euler(30f, 60f, 90f), new Vector3(3f, 2f, 1f));

		[SetUp]
		public static void SetUp()
		{
			for(int i = 0; i < Count; i++)
			{
				Source[i] = Target;
			}

			Array.Clear(Destination, 0, Count);
		}

		static void ArrayCopy()
		{
			Array.Copy(Source, 0, Destination, 0, Count);
		}

		static void MemMove()
		{
			fixed(void* sourcePtr = Source)
			fixed(void* destinationPtr = Destination)
			{
				UnsafeUtility.MemMove(destinationPtr, sourcePtr, MemSize);
			}
		}
		
		static void MemCpy()
		{
			fixed(void* sourcePtr = Source)
			fixed(void* destinationPtr = Destination)
			{
				UnsafeUtility.MemCpy(destinationPtr, sourcePtr, MemSize);
			}
		}

		static bool CheckEqual()
		{
			var isEqual = true;

			for(int i = 0; i < Count; i++)
			{
				if(Source[i] != Destination[i])
				{
					isEqual = false;
					break;
				}
			}

			return isEqual;
		}

		[BurstCompile]
		static void BurstCopyImpl([NoAlias] Matrix4x4* source, [NoAlias] Matrix4x4* destination)
		{
			for(int i = 0; i < Count; i++)
			{
				destination[i] = source[i];
			}
		}
		
		static void BurstCopy()
		{
			fixed(Matrix4x4* sourcePtr = Source)
			fixed(Matrix4x4* destinationPtr = Destination)
			{
				BurstCopyImpl(sourcePtr, destinationPtr);
			}
		}

		static void PointerCopy()
		{
			fixed(Matrix4x4* sourcePtr = Source)
			fixed(Matrix4x4* destinationPtr = Destination)
			{
				for(int i = 0; i < Count; i++)
				{
					destinationPtr[i] = sourcePtr[i];
				}
			}
		}

		[Test]
		public static void NotEqualByDefault()
		{
			Assert.IsFalse(CheckEqual());
		}

		[Test]
		public static void IsEqualAfterArrayCopy()
		{
			ArrayCopy();
			Assert.IsTrue(CheckEqual());
		}

		[Test]
		public static void IsEqualAfterMemMove()
		{
			MemMove();
			Assert.IsTrue(CheckEqual());
		}

		[Test]
		public static void IsEqualAfterMemCpy()
		{
			MemCpy();
			Assert.IsTrue(CheckEqual());
		}

		[Test]
		public static void IsEqualAfterPointerCopy()
		{
			PointerCopy();
			Assert.IsTrue(CheckEqual());
		}

		[Test]
		public static void IsEqualAfterBurstCopy()
		{
			BurstCopy();
			Assert.IsTrue(CheckEqual());
		}
		
		[Test, Performance]
		public static void PointerCopyBenchmark()
		{
			Measure.Method(PointerCopy)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void ArrayCopyBenchmark()
		{
			Measure.Method(ArrayCopy)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void MemMoveBenchmark()
		{
			Measure.Method(MemMove)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void MemCpyBenchmark()
		{
			Measure.Method(MemCpy)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}
		
		[Test, Performance]
		public static void BurstCopyBenchmark()
		{
			Measure.Method(BurstCopy)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .WarmupCount(10)
			       .Run();
		}

	}
}