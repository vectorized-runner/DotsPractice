using System.Runtime.InteropServices;
using NUnit.Framework;

namespace DotsLibrary
{
	public static unsafe class MarshalUsage
	{
		public struct MyStruct
		{
			public int FieldA;
		}
		
		[Test]
		public static void MarshalSwapMemory()
		{
			var struct1 = new MyStruct { FieldA = 10 };
			var struct2 = new MyStruct { FieldA = 20 };
			
			var struct1Ptr = Marshal.AllocHGlobal(sizeof(MyStruct));
			var struct2Ptr = Marshal.AllocHGlobal(sizeof(MyStruct));

			Marshal.StructureToPtr(struct1, struct1Ptr, false);
			Marshal.StructureToPtr(struct2, struct2Ptr, false);

			// Swap pointers
			(struct1Ptr, struct2Ptr) = (struct2Ptr, struct1Ptr);

			var temp1 = (MyStruct)Marshal.PtrToStructure(struct1Ptr, typeof(MyStruct));
			var temp2 = (MyStruct)Marshal.PtrToStructure(struct2Ptr, typeof(MyStruct));
			
			Assert.AreEqual(20, temp1.FieldA);
			Assert.AreEqual(10, temp2.FieldA);
			
			// Free memory
			Marshal.FreeHGlobal(struct1Ptr);
			Marshal.FreeHGlobal(struct2Ptr);
		}
	}
}

