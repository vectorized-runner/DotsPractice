using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace DotsLibrary
{
	public static class GenericJobs
	{
		public interface IValue
		{
			public int Value { get; }
		}

		public struct SomeItem : IValue
		{
			public int Value { get; set; }
		}

		[BurstCompile]
		struct ValueSumJob<T> : IJob where T : struct, IValue
		{
			public NativeArray<T> Items;
			public NativeReference<int> Sum;

			public void Execute()
			{
				for(int i = 0; i < Items.Length; i++)
				{
					Sum.Value += Items[i].Value;
				}
			}
		}

		[Test]
		public static void GenericSumJobWorks()
		{
			var items = new NativeArray<SomeItem>(10, Allocator.TempJob);

			for(int i = 0; i < items.Length; i++)
			{
				items[i] = new SomeItem { Value = i + 1 };
			}

			var sum = new NativeReference<int>(Allocator.TempJob);

			new ValueSumJob<SomeItem>
			{
				Items = items,
				Sum = sum
			}.Run();

			Assert.AreEqual(55, sum.Value);

			items.Dispose();
			sum.Dispose();
		}
	}
}