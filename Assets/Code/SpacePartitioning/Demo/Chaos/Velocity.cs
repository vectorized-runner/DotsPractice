using Unity.Entities;
using Unity.Mathematics;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct Velocity : IComponentData
	{
		public float3 Value;
	}
}