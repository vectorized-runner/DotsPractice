using System.Diagnostics;
using Unity.Entities;
using Unity.Mathematics;

namespace DotsLibrary.SpacePartitioning
{
	public partial class AssignRandomVelocitySystem : SystemBase
	{
		struct AssignRandomVelocityInit : IComponentData
		{
		}

		protected override void OnCreate()
		{
			EntityManager.CreateEntity(typeof(AssignRandomVelocityInit));

			RequireForUpdate<AssignRandomVelocityInit>();
		}

		protected override void OnUpdate()
		{
			if(UnityEngine.Time.frameCount < 10)
				return;
			
			var random = new Random((uint)Stopwatch.GetTimestamp());
			var maxVelocity = 100f;

			Entities.ForEach((ref Velocity velocity) =>
			        {
				        velocity.Value = random.NextFloat3Direction() * maxVelocity;
			        })
			        .Run();
			
			EntityManager.DestroyEntity(GetSingletonEntity<AssignRandomVelocityInit>());
		}
	}
}