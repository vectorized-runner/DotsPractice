using DotsLibrary.SpacePartitioning;
using Unity.Entities;
using Unity.Transforms;

namespace DotsLibrary.Collections
{
	public partial class VelocitySystem : SystemBase
	{
		protected override void OnUpdate()
		{
			var deltaTime = Time.DeltaTime;

			Entities.ForEach((ref Velocity velocity, ref Translation translation) =>
			        {
				        translation.Value += velocity.Value * deltaTime;
			        })
			        .ScheduleParallel();
		}
	}
}