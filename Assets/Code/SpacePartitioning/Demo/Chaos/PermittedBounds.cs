using Unity.Entities;
using Unity.Mathematics;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct PermittedBounds : IComponentData
	{
		public float3 Min;
		public float3 Max;
	}
}