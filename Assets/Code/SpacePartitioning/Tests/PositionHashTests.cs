using NUnit.Framework;
using Unity.Mathematics;

namespace DotsLibrary.SpacePartitioning.Tests
{
	public static class PositionHashTests
	{
		[Test]
		public static void HashForSameCellIsEqual_1()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 1f, 1f));
			var hash2 = Math.GetPositionHash(10f, new float3(2f, 2f, 2f));
			Assert.AreEqual(hash1, hash2);
		}

		[Test]
		public static void HashForSameCellIsEqual_2()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 1f, 1f));
			var hash2 = Math.GetPositionHash(10f, new float3(0.5f, 0.3f, 0.2f));
			Assert.AreEqual(hash1, hash2);
		}

		[Test]
		public static void HashForSameCellIsEqual_3()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(15f, 13f, 21f));
			var hash2 = Math.GetPositionHash(10f, new float3(12f, 19.34f, 27.4353f));
			Assert.AreEqual(hash1, hash2);
		}

		[Test]
		public static void HashForDifferentCellIsNotEqual_1()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 5f, 7f));
			var hash2 = Math.GetPositionHash(10f, new float3(11f, 5f, 7f));
			Assert.AreNotEqual(hash1, hash2);
		}

		[Test]
		public static void HashForDifferentCellIsNotEqual_2()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 50f, 7f));
			var hash2 = Math.GetPositionHash(10f, new float3(11f, 5f, 7f));
			Assert.AreNotEqual(hash1, hash2);
		}

		[Test]
		public static void MinBoundIsIncluded()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 1f, 1f));
			var hash2 = Math.GetPositionHash(10f, new float3(0f, 0f, 0f));
			Assert.AreEqual(hash1, hash2);
		}

		[Test]
		public static void MaxBoundIsNotIncluded()
		{
			var hash1 = Math.GetPositionHash(10f, new float3(1f, 1f, 1f));
			var hash2 = Math.GetPositionHash(10f, new float3(10f, 10f, 10f));
			Assert.AreNotEqual(hash1, hash2);
		}
	}
}