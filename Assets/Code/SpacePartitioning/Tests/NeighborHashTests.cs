using NUnit.Framework;
using Unity.Collections;
using Unity.Mathematics;

namespace DotsLibrary.SpacePartitioning.Tests
{
	public static class NeighborHashTests
	{
		[Test]
		public static void CheckNeighborHashes()
		{
			var buffer = new NativeArray<uint>(27, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
			var cellSize = 10f;
			Math.GetNeighborHashes(buffer, 0, cellSize, new float3(15f, 15f, 15f));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 5f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 5f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 5f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 5f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 5f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 5f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 5f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 5f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 5f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 15f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 15f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 15f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 15f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 15f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 15f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 15f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 15f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 15f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 25f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 25f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(5f, 25f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 25f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 25f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(15f, 25f, 25f))));

			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 25f, 5f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 25f, 15f))));
			Assert.IsTrue(buffer.Contains(Math.GetPositionHash(cellSize, new float3(25f, 25f, 25f))));
			
			Assert.AreEqual(27, buffer.Length);
		}
	}
}