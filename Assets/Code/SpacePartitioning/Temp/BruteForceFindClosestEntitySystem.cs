using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLibrary.SpacePartitioning
{
	public partial class BruteForceFindClosestEntitySystem : SystemBase
	{
		protected override void OnUpdate()
		{
			var translationData = GetComponentDataFromEntity<Translation>(true);
			var translationEntities = GetEntityQuery(ComponentType.ReadOnly<Translation>(),
					ComponentType.ReadOnly<BruteForceFindClosestEntity>())
				.ToEntityArrayAsync(Allocator.TempJob, out var translationJobHandle);

			Dependency = JobHandle.CombineDependencies(Dependency, translationJobHandle);

			Entities.ForEach((Entity entity, ref BruteForceFindClosestEntity bruteForceFindClosestEntity,
			                  in Translation translation) =>
			        {
				        var closestEntity = Entity.Null;
				        var closestDistSq = float.PositiveInfinity;
				        var position = translation.Value;

				        for(int i = 0; i < translationEntities.Length; i++)
				        {
					        var otherEntity = translationEntities[i];

					        if(otherEntity == entity)
						        continue;

					        var otherPosition = translationData[otherEntity].Value;
					        var distSq = math.distancesq(position, otherPosition);
					        if(distSq < closestDistSq)
					        {
						        closestDistSq = distSq;
						        closestEntity = otherEntity;
					        }

					        bruteForceFindClosestEntity.Value = closestEntity;
				        }
			        })
			        .WithName("BruteForceFindClosestEntityJob")
			        .WithReadOnly(translationData)
			        .WithNativeDisableParallelForRestriction(translationEntities)
			        .WithDisposeOnCompletion(translationEntities)
			        .ScheduleParallel();
		}
	}
}