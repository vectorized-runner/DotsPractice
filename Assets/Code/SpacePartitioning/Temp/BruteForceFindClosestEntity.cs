using Unity.Entities;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct BruteForceFindClosestEntity : IComponentData
	{
		public Entity Value;
	}
}

