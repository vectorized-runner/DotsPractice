using Unity.Entities;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct SpacePartitionData : IComponentData
	{
		public float CellSize;
	}
}