using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace DotsLibrary.SpacePartitioning
{
	public partial class SpacePartitioningDebugDrawSystem : SystemBase
	{
		protected override void OnCreate()
		{
			RequireForUpdate<DebugDrawInit>();
			RequireForUpdate<SpacePartitionData>();
		}

		protected override void OnUpdate()
		{
			var translationData = GetComponentDataFromEntity<Translation>(true);

			Entities
				.ForEach((in PartitionFindClosestEntity findClosestEntity, in Translation translation) =>
				{
					if(findClosestEntity.Entity == Entity.Null)
						return;
					var start = translation.Value;
					var end = translationData[findClosestEntity.Entity].Value;
					Debug.DrawLine(start, end, Color.yellow);
				})
				.WithReadOnly(translationData)
				.Run();
		}
	}
}