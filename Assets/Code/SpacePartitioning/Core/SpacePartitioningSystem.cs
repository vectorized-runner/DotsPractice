using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

namespace DotsLibrary.SpacePartitioning
{
	public partial class SpacePartitioningSystem : SystemBase
	{
		public JobHandle PartitionHandle { get; private set; }
		public NativeParallelMultiHashMap<uint, Entity> PartitionEntities;
		NativeList<JobHandle> ReadJobs;
		const int InitialCapacity = 10000;

		protected override void OnCreate()
		{
			ReadJobs = new NativeList<JobHandle>(Allocator.Persistent);
			PartitionEntities = new NativeParallelMultiHashMap<uint, Entity>(InitialCapacity, Allocator.Persistent);
			RequireForUpdate<SpacePartitionData>();
		}

		protected override void OnDestroy()
		{
			ReadJobs.Dispose();
			PartitionEntities.Dispose();
		}

		public void AddReadDependency(JobHandle jobHandle)
		{
			ReadJobs.Add(jobHandle);
		}

		protected override void OnUpdate()
		{
			// Other systems that use PartitionEntities needs to register their dependency to ReadJobs
			JobHandle.CompleteAll(ReadJobs);
			ReadJobs.Clear();
			
			// Reconstruct every frame
			PartitionEntities.Clear();
			var parallelWriter = PartitionEntities.AsParallelWriter();
			var cellSize = GetSingleton<SpacePartitionData>().CellSize;

			PartitionHandle = Entities
			                  .WithAll<SpacePartitionUnit>()
			                  .ForEach((Entity entity, in Translation translation) =>
			                  {
				                  var hash = Math.GetPositionHash(cellSize, translation.Value);
				                  parallelWriter.Add(hash, entity);
			                  })
			                  .WithName("PositionHashingJob")
			                  .ScheduleParallel(Dependency);

			Dependency = PartitionHandle;
		}
	}
}