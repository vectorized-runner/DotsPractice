using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Profiling;
using Unity.Transforms;

namespace DotsLibrary.SpacePartitioning
{
	[UpdateAfter(typeof(SpacePartitioningSystem))]
	public partial class SpacePartitioningFindClosestEntitySystem : SystemBase
	{
		NativeArray<uint> NeighborHashesBuffer;
		NativeArray<UnsafeList<Entity>> NeighborEntitiesBuffer;

		protected override unsafe void OnCreate()
		{
			NeighborHashesBuffer = new NativeArray<uint>(Math.NeighborCount * JobsUtility.MaxJobThreadCount, Allocator.Persistent);

			NeighborEntitiesBuffer = new NativeArray<UnsafeList<Entity>>(JobsUtility.MaxJobThreadCount, Allocator.Persistent);
			for(int i = 0; i < JobsUtility.MaxJobThreadCount; i++)
			{
				NeighborEntitiesBuffer[i] = new UnsafeList<Entity>(100, Allocator.Persistent);
			}
		}

		protected override void OnDestroy()
		{
			NeighborHashesBuffer.Dispose();

			for(int i = 0; i < JobsUtility.MaxJobThreadCount; i++)
			{
				NeighborEntitiesBuffer[i].Dispose();
			}

			NeighborEntitiesBuffer.Dispose();
		}

		protected override unsafe void OnUpdate()
		{
			var spacePartitioningSystem = World.GetExistingSystem<SpacePartitioningSystem>();
			var partitionEntities = spacePartitioningSystem.PartitionEntities;
			var cellSize = GetSingleton<SpacePartitionData>().CellSize;
			var translationData = GetComponentDataFromEntity<Translation>(true);
			var neighborHashes = NeighborHashesBuffer;
			var neighborEntitiesBuffer = NeighborEntitiesBuffer;
			var getNeighborHashesMarker = new ProfilerMarker("GetNeighborHashes");
			var collectEntitiesMarker = new ProfilerMarker("CollectEntities");
			var getClosestEntityMarker = new ProfilerMarker("GetClosestEntity");

			Dependency = JobHandle.CombineDependencies(Dependency, spacePartitioningSystem.PartitionHandle);

			Entities
				.WithAll<SpacePartitionUnit>()
				.ForEach((int nativeThreadIndex, Entity entity, ref PartitionFindClosestEntity partitionFindClosest,
				          in Translation translation) =>
				{
					getNeighborHashesMarker.Begin();

					var position = translation.Value;
					var startIndex = nativeThreadIndex * Math.NeighborCount;
					var endIndex = startIndex + Math.NeighborCount;
					Math.GetNeighborHashes(neighborHashes, startIndex, cellSize, position);

					getNeighborHashesMarker.End();
					collectEntitiesMarker.Begin();

					ref var neighborEntities = ref UnsafeUtility.ArrayElementAsRef<UnsafeList<Entity>>(neighborEntitiesBuffer.GetUnsafePtr(), nativeThreadIndex);
					for(int i = startIndex; i < endIndex; i++)
					{
						CollectEntities(ref neighborEntities, neighborHashes[i], partitionEntities);
					}

					collectEntitiesMarker.End();
					getClosestEntityMarker.Begin();

					var closestEntity = Math.GetClosestEntity(entity, position, ref neighborEntities, translationData);
					partitionFindClosest.Entity = closestEntity;
					
					neighborEntities.Clear();

					getClosestEntityMarker.End();
				})
				.WithName("SpacePartitioningFindClosestEntityJob")
				.WithNativeDisableParallelForRestriction(neighborHashes)
				.WithReadOnly(partitionEntities)
				.WithReadOnly(translationData)
				.ScheduleParallel();

			spacePartitioningSystem.AddReadDependency(Dependency);
		}

		static void CollectEntities(ref UnsafeList<Entity> entities, uint hash,
		                            NativeParallelMultiHashMap<uint, Entity> entitySource)
		{
			if(entitySource.TryGetFirstValue(hash, out var entity, out var iterator))
			{
				entities.Add(entity);

				while(entitySource.TryGetNextValue(out entity, ref iterator))
				{
					entities.Add(entity);
				}
			}
		}
	}
}