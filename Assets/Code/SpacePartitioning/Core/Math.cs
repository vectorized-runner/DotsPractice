using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DotsLibrary.SpacePartitioning
{
	public static class Math
	{
		public const int NeighborCount = 27;
		
		public static void GetNeighborHashes(NativeArray<uint> buffer, int writeStartIndex, float cellSize, float3 position)
		{
			var index = writeStartIndex;
			
			for(float xOffset = -cellSize; xOffset <= cellSize; xOffset += cellSize)
			{
				for(float yOffset = -cellSize; yOffset <= cellSize; yOffset += cellSize)
				{
					for(float zOffset = -cellSize; zOffset <= cellSize; zOffset += cellSize)
					{
						var sample = position + new float3(xOffset, yOffset, zOffset);
						var hash = GetPositionHash(cellSize, sample);
						buffer[index] = hash;
						index++;
					}
				}
			}
		}

		public static uint GetPositionHash(float cellSize, float3 position)
		{
			var v = (int3)math.floor(position / cellSize);
			var hash = math.hash(v);
			return hash;
		}
		
		public static Entity GetClosestEntity(Entity exclude, float3 position, ref UnsafeList<Entity> entities,
		                               ComponentLookup<Translation> translationData)
		{
			var closestEntity = Entity.Null;
			var closestDistanceSq = float.PositiveInfinity;

			for(int i = 0; i < entities.Length; i++)
			{
				var otherEntity = entities[i];

				if(otherEntity == exclude)
					continue;

				var otherPosition = translationData[otherEntity].Value;
				var distSq = math.distancesq(position, otherPosition);
				if(distSq < closestDistanceSq)
				{
					closestDistanceSq = distSq;
					closestEntity = otherEntity;
				}
			}

			return closestEntity;
		}
	}
}