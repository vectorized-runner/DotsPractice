using Unity.Entities;

namespace DotsLibrary.SpacePartitioning
{
	[GenerateAuthoringComponent]
	public struct PartitionFindClosestEntity : IComponentData
	{
		public Entity Entity;
	}
}

