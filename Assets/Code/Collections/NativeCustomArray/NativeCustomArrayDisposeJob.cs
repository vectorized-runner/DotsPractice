using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;

namespace DotsPractice.Collections
{
	struct NativeCustomArrayDispose
	{
		[NativeDisableUnsafePtrRestriction]
		public unsafe void* Buffer;

		public Allocator Allocator;

		public unsafe void Dispose()
		{
			UnsafeUtility.Free(Buffer, Allocator);
		}
	}

	[BurstCompile]
	struct NativeCustomArrayDisposeJob : IJob
	{
		public NativeCustomArrayDispose Data;

		public void Execute()
		{
			Data.Dispose();
		}
	}
}