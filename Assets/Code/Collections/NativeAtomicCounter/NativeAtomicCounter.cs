using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs.LowLevel.Unsafe;
// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable InconsistentNaming
namespace NativeCollections
{
	[StructLayout(LayoutKind.Sequential)]
	[NativeContainer]
	public unsafe struct NativeAtomicCounter
	{
		[NativeDisableUnsafePtrRestriction]
		int* Buffer;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
		AtomicSafetyHandle m_Safety;

		[NativeSetClassTypeToNullOnSchedule]
		DisposeSentinel m_DisposeSentinel;
#endif

		Allocator Allocator;
		
		const int IntsPerCacheLine = JobsUtility.CacheLineSize / sizeof(int);

		public NativeAtomicCounter(Allocator allocator, int initialCount = 0)
		{
			Allocator = allocator;
			
			// If cache line size is 64, we use 2 ints per thread, write to the first int per thread, second one isn't used but required to prevent false sharing
			var size = UnsafeUtility.SizeOf<int>() * JobsUtility.MaxJobThreadCount * IntsPerCacheLine;
			var alignment = UnsafeUtility.AlignOf<int>();
			Buffer = (int*)UnsafeUtility.Malloc(size, alignment, allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Create(out m_Safety, out m_DisposeSentinel, 0, allocator);
#endif
			Count = initialCount;
		}

		public int Count
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckReadAccess();
#endif
				// Sum up integers in all threads
				int value = 0;
				for(int i = 0; i < JobsUtility.MaxJobThreadCount; i++)
				{
					value += Buffer[IntsPerCacheLine * i];
				}

				return value;
			}
			[WriteAccessRequired]
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckWriteAccess();
#endif
				// Clear all other written values, since this is single-threaded write
				for(int i = 0; i < JobsUtility.MaxJobThreadCount; i++)
				{
					Buffer[IntsPerCacheLine * i] = 0;
				}

				*Buffer = value;
			}
		}

		public bool IsCreated => Buffer != null;

		public void Dispose()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
			UnsafeUtility.Free(Buffer, Allocator);
			Allocator = Allocator.Invalid;
			Buffer = null;
		}

		[WriteAccessRequired]
		public void Increment()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAccess();
#endif
			(*Buffer)++;
		}

		[WriteAccessRequired]
		public void Decrement()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAccess();
#endif
			(*Buffer)--;
		}

		public ParallelWriter AsParallelWriter()
		{
			return new ParallelWriter(this);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckReadAccess()
		{
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckWriteAccess()
		{
			AtomicSafetyHandle.CheckWriteAndThrow(m_Safety);
		}

		[NativeContainer]
		[NativeContainerIsAtomicWriteOnly]
		public struct ParallelWriter
		{
			[NativeDisableUnsafePtrRestriction]
			int* Buffer;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle m_Safety;
#endif
	
			// Thread index will be injected by the job system
			[NativeSetThreadIndex]
			int m_ThreadIndex;

			public ParallelWriter(NativeAtomicCounter counter)
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckWriteAndThrow(counter.m_Safety);
				m_Safety = counter.m_Safety;
				AtomicSafetyHandle.UseSecondaryVersion(ref m_Safety);
#endif
				Buffer = counter.Buffer;
				m_ThreadIndex = 0;
			}

			[WriteAccessRequired]
			public void Increment()
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckWriteAccess();
#endif
				Buffer[IntsPerCacheLine * m_ThreadIndex]++;
			}

			[WriteAccessRequired]
			public void Decrement()
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckWriteAccess();
#endif
				Buffer[IntsPerCacheLine * m_ThreadIndex]--;
			}

			[WriteAccessRequired]
			public void Add(int amount)
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckWriteAccess();
#endif
				Buffer[IntsPerCacheLine * m_ThreadIndex] += amount;
			}

			[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
			void CheckWriteAccess()
			{
				AtomicSafetyHandle.CheckWriteAndThrow(m_Safety);
			}
		}
	}
}