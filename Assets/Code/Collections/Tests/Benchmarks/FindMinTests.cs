using System.Diagnostics;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.PerformanceTesting;

namespace DotsLibrary.Collections.Tests.Benchmarks
{
	[BurstCompile]
	public static class FindMinTests
	{
		static void AddRandomElements(NativeList<float> list, int count, ref Random random)
		{
			for(int i = 0; i < count; i++)
			{
				list.Add(random.NextFloat());
			}
		}

		static void AddRandomElements(NativeMinHeapSingleData<float> minHeapSingleData, int count, ref Random random)
		{
			for(int i = 0; i < count; i++)
			{
				minHeapSingleData.Push(random.NextFloat());
			}
		}

		[BurstCompile]
		struct ListFindMinJob : IJob
		{
			public NativeReference<float> Min;
			public NativeList<float> List;
			public Random Random;

			public void Execute()
			{
				for(int i = 0; i < StepCount; i++)
				{
					AddRandomElements(List, ElementsPerStep, ref Random);
					var minIndex = FindMinIndexMethod(List);
					Min.Value = List[minIndex];
					List.RemoveAtSwapBack(minIndex);
				}
			}
		}

		[BurstCompile]
		struct MinHeapFindMinJob : IJob
		{
			public NativeReference<float> Min;
			public NativeMinHeapSingleData<float> MinHeapSingleData;
			public Random Random;

			public void Execute()
			{
				for(int i = 0; i < StepCount; i++)
				{
					AddRandomElements(MinHeapSingleData, ElementsPerStep, ref Random);
					Min.Value = MinHeapSingleData.Pop();
				}
			}
		}

		static int FindMinIndexMethod(NativeList<float> list)
		{
			var minIndex = -1;
			var minValue = float.MaxValue;

			for(int i = 0; i < list.Length; i++)
			{
				if(list[i] < minValue)
				{
					minValue = list[i];
					minIndex = i;
				}
			}

			return minIndex;
		}

		const int StepCount = 25;
		const int ElementsPerStep = 200;
		const int IterationCount = 10;

		[Test, Performance]
		public static void FindMinNativeList()
		{
			NativeList<float> list = default;
			NativeReference<float> min = default;
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       new ListFindMinJob
				       {
					       List = list,
					       Min = min,
					       Random = random,
				       }.Run();

				       var minItem = min.Value;
			       })
			       .SetUp(() =>
			       {
				       list = new NativeList<float>(StepCount * ElementsPerStep, Allocator.TempJob);
				       min = new NativeReference<float>(Allocator.TempJob);
			       })
			       .CleanUp(() =>
			       {
				       list.Dispose();
				       min.Dispose();
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void FindMinNativeMinHeap()
		{
			NativeMinHeapSingleData<float> minHeapSingleData = default;
			NativeReference<float> min = default;
			var random = new Random((uint)Stopwatch.GetTimestamp());

			Measure.Method(() =>
			       {
				       new MinHeapFindMinJob
				       {
					       MinHeapSingleData = minHeapSingleData,
					       Min = min,
					       Random = random,
				       }.Run();

				       var minItem = min.Value;
			       })
			       .SetUp(() =>
			       {
				       minHeapSingleData = new NativeMinHeapSingleData<float>(Allocator.TempJob, StepCount * ElementsPerStep);
				       min = new NativeReference<float>(Allocator.TempJob);
			       })
			       .CleanUp(() =>
			       {
				       minHeapSingleData.Dispose();
				       min.Dispose();
			       })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(IterationCount)
			       .MeasurementCount(10)
			       .Run();
		}
	}
}