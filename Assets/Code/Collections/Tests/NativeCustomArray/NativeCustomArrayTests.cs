using System;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.TestTools;

namespace DotsPractice.Collections.Tests
{
	public static class NativeCustomArrayTests
	{
		public static void ExpectInvalidOperationException()
		{
			LogAssert.Expect(LogType.Exception, new Regex("System::InvalidOperationException*"));
		}
		
		[BurstCompile]
		struct SimpleWriteJob : IJob
		{
			public int Value1;
			public int Value2;
			public NativeCustomArray<int> CustomArray;

			public void Execute()
			{
				for(int i = 0; i < CustomArray.Length; i++)
				{
					CustomArray[i] = Value1 * Value2;
				}
			}
		}

		[BurstCompile]
		struct SimpleWriteOnlyWriteJob : IJob
		{
			public int Value1;
			public int Value2;

			[WriteOnly]
			public NativeCustomArray<int> CustomArray;

			public void Execute()
			{
				for(int i = 0; i < CustomArray.Length; i++)
				{
					CustomArray[i] = Value1 * Value2;
				}
			}
		}

		[BurstCompile]
		struct SimpleReadOnlyWriteJob : IJob
		{
			public int Value1;
			public int Value2;

			[ReadOnly]
			public NativeCustomArray<int> CustomArray;

			public void Execute()
			{
				for(int i = 0; i < CustomArray.Length; i++)
				{
					CustomArray[i] = Value1 * Value2;
				}
			}
		}

		[BurstCompile]
		struct SimpleWriteOnlyReadJob : IJob
		{
			public int Value1;

			[WriteOnly]
			public NativeCustomArray<int> CustomArray;

			public void Execute()
			{
				for(int i = 0; i < CustomArray.Length; i++)
				{
					Value1 = CustomArray[i];
				}
			}
		}

		[BurstCompile]
		struct SimpleReadOnlyReadJob : IJob
		{
			public int Value1;

			[ReadOnly]
			public NativeCustomArray<int> CustomArray;

			public void Execute()
			{
				for(int i = 0; i < CustomArray.Length; i++)
				{
					Value1 = CustomArray[i];
				}
			}
		}

		[BurstCompile]
		struct TrickyJob : IJob
		{
			public NativeCustomArray<int> First;
			public NativeCustomArray<int> Second;

			public void Execute()
			{
				for(int i = 0; i < First.Length; i++)
				{
					First[i] = Second[i];
				}
			}
		}

		[BurstCompile]
		struct ParallelJob : IJobParallelFor
		{
			public NativeCustomArray<int> Array;

			public void Execute(int index)
			{
				Array[index] = index;
			}
		}

		[BurstCompile]
		struct ParallelBadJob : IJobParallelFor
		{
			public NativeCustomArray<int> Array;

			public void Execute(int index)
			{
				Array[index + 1] = index;
			}
		}

		[BurstCompile]
		struct DeallocateOnCompletionJob : IJob
		{
			public int Value;

			[DeallocateOnJobCompletion]
			public NativeCustomArray<int> Array;

			public void Execute()
			{
				for(int i = 0; i < Array.Length; i++)
				{
					Array[i] = i + Value;
				}
			}
		}

		[Test]
		public static void IsNotCreatedByDefault()
		{
			NativeCustomArray<int> array = default;
			Assert.IsFalse(array.IsCreated);
		}

		[Test]
		public static void IsCreatedAfterConstructorCall()
		{
			var array = new NativeCustomArray<int>(10, Allocator.Temp);
			Assert.IsTrue(array.IsCreated);
			array.Dispose();
		}

		[Test]
		public static void IsNotCreatedAfterDisposed()
		{
			var array = new NativeCustomArray<int>(10, Allocator.Temp);
			array.Dispose();
			Assert.IsFalse(array.IsCreated);
		}

		[Test]
		public static void DefaultInitializedAfterClearMemoryRequest()
		{
			var array = new NativeCustomArray<int>(10, Allocator.Temp, NativeArrayOptions.ClearMemory);

			for(int i = 0; i < array.Length; i++)
			{
				Assert.AreEqual(default(int), array[i]);
			}

			array.Dispose();
		}

		[Test]
		public static void CanBeWrittenWithoutJob()
		{
			var array = new NativeCustomArray<int>(10, Allocator.Temp, NativeArrayOptions.ClearMemory);

			for(int i = 0; i < array.Length; i++)
			{
				array[i] = i;
			}

			for(int i = 0; i < array.Length; i++)
			{
				Assert.AreEqual(i, array[i]);
			}

			array.Dispose();
		}

		[Test]
		public static void CanBeWrittenWhenRun()
		{
			var array = new NativeCustomArray<int>(10, Allocator.TempJob);

			new SimpleWriteJob
			{
				Value1 = 5,
				Value2 = 10,
				CustomArray = array
			}.Run();

			for(int i = 0; i < array.Length; i++)
			{
				Assert.AreEqual(50, array[i]);
			}

			array.Dispose();
		}

		[Test]
		public static void CanBeWrittenWhenScheduled()
		{
			var array = new NativeCustomArray<int>(10, Allocator.TempJob);

			var jobHandle = new SimpleWriteJob
			{
				Value1 = 5,
				Value2 = 10,
				CustomArray = array
			}.Schedule();

			jobHandle.Complete();

			for(int i = 0; i < array.Length; i++)
			{
				Assert.AreEqual(50, array[i]);
			}

			array.Dispose();
		}

		[Test]
		public static void CanBeWrittenWhenDeclaredAsWriteOnly()
		{
			var array = new NativeCustomArray<int>(10, Allocator.TempJob);

			new SimpleWriteOnlyWriteJob
			{
				Value1 = 5,
				Value2 = 10,
				CustomArray = array
			}.Run();

			for(int i = 0; i < array.Length; i++)
			{
				Assert.AreEqual(50, array[i]);
			}

			array.Dispose();
		}

		[Test]
		public static void CantBeWrittenWhenDeclaredAsReadOnly()
		{
			ExpectInvalidOperationException();

			var array = new NativeCustomArray<int>(10, Allocator.TempJob);

			new SimpleReadOnlyWriteJob
			{
				Value1 = 5,
				Value2 = 10,
				CustomArray = array
			}.Run();

			array.Dispose();
		}

		[Test]
		public static void CanBeReadWhenDeclaredAsReadOnly()
		{
			Assert.DoesNotThrow(() =>
			{
				var array = new NativeCustomArray<int>(10, Allocator.TempJob);

				new SimpleReadOnlyReadJob
				{
					Value1 = 5,
					CustomArray = array
				}.Run();

				array.Dispose();
			});
		}

		[Test]
		public static void CantBeReadWhenDeclaredAsWriteOnly()
		{
			ExpectInvalidOperationException();

			var array = new NativeCustomArray<int>(10, Allocator.TempJob);

			new SimpleWriteOnlyReadJob
			{
				Value1 = 5,
				CustomArray = array
			}.Run();

			array.Dispose();
		}

		[Test]
		public static void CantReadAndWriteFromSameArray()
		{
			NativeCustomArray<int> array = default;
			
			Assert.Throws<InvalidOperationException>(() =>
			{
				array = new NativeCustomArray<int>(10, Allocator.TempJob);

				new TrickyJob
				{
					First = array,
					Second = array
				}.Run();
			});

			array.Dispose();
		}

		[Test]
		public static void CanBeWrittenInIJobParallelFor()
		{
			var arrayLength = 256;
			var array = new NativeCustomArray<int>(arrayLength, Allocator.TempJob);

			var jobHandle = new ParallelJob
			{
				Array = array,
			}.Schedule(arrayLength, 64);

			jobHandle.Complete();

			for(int i = 0; i < arrayLength; i++)
			{
				Assert.AreEqual(array[i], i);
			}

			array.Dispose();
		}

		[Test]
		public static void DeallocateOnCompletion()
		{
			var array = new NativeCustomArray<int>(10, Allocator.TempJob);
			var jobHandle = new DeallocateOnCompletionJob
			{
				Value = 0,
				Array = array
			}.Schedule();

			jobHandle.Complete();

			Assert.Throws<ObjectDisposedException>(() =>
			{
				var value = array[0];
			});
		}

		[Test]
		public static void CantReadOutOfRange()
		{
			Assert.Throws<IndexOutOfRangeException>(() =>
			{
				var value = new NativeCustomArray<int>(10, Allocator.Temp)[11];
			});
		}

		[Test]
		public static void CantWriteOutOfRange()
		{
			Assert.Throws<IndexOutOfRangeException>(() =>
			{
				var array = new NativeCustomArray<int>(10, Allocator.Temp);
				array[11] = 5;
			});
		}

		[Test]
		public static void SanityException()
		{
			Assert.Throws<NullReferenceException>(() =>
			{
				NativeArray<int> arr = default;
				var value = arr[0];
			});
		}
	}
}