using System;
using System.Collections.Generic;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace NativeCollections.Tests
{
	struct IntComparer : IComparer<int>
	{
		public int Compare(int x, int y)
		{
			return x.CompareTo(y);
		}
	}

	[BurstCompile]
	struct EmptyNativeHeapJob : IJob
	{
		public NativeMinHeap<int, IntComparer> MinHeap;

		public void Execute()
		{
			// Nothing
		}
	}

	public static class NativeMinHeapTests
	{
		static IntComparer IntComparer = new IntComparer();

		[Test]
		public static void IsCreatedAfterConstructor()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			Assert.IsTrue(heap.IsCreated);
		}

		[Test]
		public static void NotCreatedByDefault()
		{
			NativeMinHeap<int, IntComparer> minHeap = default;
			Assert.IsFalse(minHeap.IsCreated);
		}

		[Test]
		public static void NotCreatedAfterDisposed()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Dispose();
			Assert.IsFalse(heap.IsCreated);
		}

		[Test]
		public static void LengthIsZeroByDefault()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			Assert.AreEqual(0, heap.Count);
		}

		[Test]
		public static void LengthIncreasesByOneEachPush()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);

			heap.Add(1);
			Assert.AreEqual(1, heap.Count);

			heap.Add(23498);
			Assert.AreEqual(2, heap.Count);

			heap.Add(-1238934);
			Assert.AreEqual(3, heap.Count);

			heap.Add(3945830);
			Assert.AreEqual(4, heap.Count);

			heap.Dispose();
		}

		[Test]
		public static void DoesNotThrowWhenUsedInJob()
		{
			Assert.DoesNotThrow(() =>
			{
				var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.TempJob);

				new EmptyNativeHeapJob
				{
					MinHeap = heap
				}.Run();

				heap.Dispose();
			});
		}

		[Test]
		public static void NativeHeapImpl_1()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			Assert.AreEqual(10, heap.Peek());
		}

		[Test]
		public static void NativeHeapImpl_2()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(5);
			Assert.AreEqual(5, heap.Peek());
		}

		[Test]
		public static void NativeHeapImpl_3()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(5);
			heap.Add(10);
			Assert.AreEqual(5, heap.Peek());
		}

		[Test]
		public static void NativeHeapImpl_4()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(-1);
			heap.Add(1);
			heap.Add(5);
			Assert.AreEqual(-1, heap.Peek());
		}

		[Test]
		public static void NativeHeapImpl_5()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			Assert.IsTrue(heap.IsEmpty);
		}

		[Test]
		public static void NativeHeapImpl_6()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(1);
			Assert.IsFalse(heap.IsEmpty);
		}

		[Test]
		public static void NativeHeapImpl_7()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			Assert.Throws<IndexOutOfRangeException>(() => Debug.Log(heap.Peek()));
		}

		[Test]
		public static void NativeHeapImpl_8()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(-1);
			heap.Add(1);
			heap.Add(5);
			heap.Add(-213235);
			heap.Add(100);
			heap.Add(-200);
			heap.Add(1342456);
			Assert.AreEqual(-213235, heap.Peek());
		}

		[Test]
		public static void NativeHeapImpl_9()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(5);

			Assert.AreEqual(5, heap.Remove());
			Assert.AreEqual(10, heap.Remove());
		}

		[Test]
		public static void NativeHeapImpl_10()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			Assert.AreEqual(10, heap.Remove());
		}

		[Test]
		public static void NativeHeapImpl_11()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(-40);
			heap.Add(1000);
			heap.Add(5);
			Assert.AreEqual(-40, heap.Remove());
			Assert.AreEqual(5, heap.Remove());
			Assert.AreEqual(10, heap.Remove());
			Assert.AreEqual(1000, heap.Remove());
			Assert.IsTrue(heap.IsEmpty);
		}

		[Test]
		public static void NativeHeapImpl_12()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(-40);
			heap.Add(1000);
			heap.Add(5);
			heap.Add(int.MaxValue);
			heap.Add(int.MinValue);
			heap.Add(123125);
			heap.Add(-123125);

			Assert.AreEqual(int.MinValue, heap.Remove());
			Assert.AreEqual(-123125, heap.Remove());
			Assert.AreEqual(-40, heap.Remove());
			Assert.AreEqual(5, heap.Remove());
			Assert.AreEqual(10, heap.Remove());
			Assert.AreEqual(1000, heap.Remove());
			Assert.AreEqual(123125, heap.Remove());
			Assert.AreEqual(int.MaxValue, heap.Remove());
			Assert.IsTrue(heap.IsEmpty);
		}

		[Test]
		public static void NativeHeapImpl_13()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.Add(10);
			heap.Add(-40);
			heap.Add(1000);
			heap.Add(5);

			Assert.AreEqual(4, heap.Count);

			heap.Remove();
			Assert.AreEqual(3, heap.Count);

			heap.Remove();
			Assert.AreEqual(2, heap.Count);

			heap.Remove();
			Assert.AreEqual(1, heap.Count);

			heap.Remove();
			Assert.AreEqual(0, heap.Count);
		}

		[Test]
		public static void BuildHeap()
		{
			var list = new NativeList<int>(Allocator.Temp)
			{
				92, 47, 21, 20, 12, 45, 63, 61, 17, 55, 37, 25, 64, 83, 73
			};
			
			Assert.IsTrue(list.IsCreated);

			var heap = new NativeMinHeap<int, IntComparer>(list, IntComparer, Allocator.Temp);

			Assert.AreEqual(15, heap.Count);
			Assert.AreEqual(12, heap.Remove());
			Assert.AreEqual(17, heap.Remove());
			Assert.AreEqual(20, heap.Remove());
			Assert.AreEqual(21, heap.Remove());
			Assert.AreEqual(25, heap.Remove());
			Assert.AreEqual(37, heap.Remove());
			Assert.AreEqual(45, heap.Remove());
			Assert.AreEqual(47, heap.Remove());
			Assert.AreEqual(55, heap.Remove());
			Assert.AreEqual(61, heap.Remove());
			Assert.AreEqual(63, heap.Remove());
			Assert.AreEqual(64, heap.Remove());
			Assert.AreEqual(73, heap.Remove());
			Assert.AreEqual(83, heap.Remove());
			Assert.AreEqual(92, heap.Remove());
			Assert.IsTrue(heap.IsEmpty);
		}
		
		[Test]
		public static void PushRange()
		{
			var list1 = new NativeList<int>(Allocator.Temp)
			{
				92, 47, 21, 20, 12
			};
			
			var list2 = new NativeList<int>(Allocator.Temp)
			{
				45, 63, 61, 17, 55
			};
			
			var list3 = new NativeList<int>(Allocator.Temp)
			{
				37, 25, 64, 83, 73
			};
			
			Assert.IsTrue(list1.IsCreated);
			Assert.IsTrue(list2.IsCreated);
			Assert.IsTrue(list3.IsCreated);

			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.Temp);
			heap.AddRange(list1);
			heap.AddRange(list2);
			heap.AddRange(list3);

			Assert.AreEqual(15, heap.Count);
			Assert.AreEqual(12, heap.Remove());
			Assert.AreEqual(17, heap.Remove());
			Assert.AreEqual(20, heap.Remove());
			Assert.AreEqual(21, heap.Remove());
			Assert.AreEqual(25, heap.Remove());
			Assert.AreEqual(37, heap.Remove());
			Assert.AreEqual(45, heap.Remove());
			Assert.AreEqual(47, heap.Remove());
			Assert.AreEqual(55, heap.Remove());
			Assert.AreEqual(61, heap.Remove());
			Assert.AreEqual(63, heap.Remove());
			Assert.AreEqual(64, heap.Remove());
			Assert.AreEqual(73, heap.Remove());
			Assert.AreEqual(83, heap.Remove());
			Assert.AreEqual(92, heap.Remove());
			Assert.IsTrue(heap.IsEmpty);
		}

		[BurstCompile]
		struct OperationsJob : IJob
		{
			public NativeMinHeap<int, IntComparer> MinHeap;

			public void Execute()
			{
				MinHeap.Add(10);
				MinHeap.Add(-1000);
				MinHeap.Peek();
				MinHeap.TryPeek(out _);
				MinHeap.Remove();
				MinHeap.TryRemove(out _);
			}
		}

		[Test]
		public static void CanDoOperationsInsideJob()
		{
			var heap = new NativeMinHeap<int, IntComparer>(IntComparer, Allocator.TempJob);

			Assert.DoesNotThrow(() =>
			{
				new OperationsJob
				{
					MinHeap = heap
				}.Run();
			});

			heap.Dispose();
		}
	}
}