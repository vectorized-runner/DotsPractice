using NUnit.Framework;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace DotsLibrary.Collections.Tests
{
	public static class UnsafeListUsageTests
	{
		[Test]
		public static void CanAddToListWhenAccessedThroughIndexer()
		{
			Assert.DoesNotThrow(() =>
			{
				var array = new NativeArray<UnsafeList<int>>(1, Allocator.Temp);
				array[0] = new UnsafeList<int>(10, Allocator.Temp);
				array[0].Add(1);
				array[0].Dispose();
				array.Dispose();
			});
		}
		
		[Test]
		public static unsafe void CanAddToListWhenAccessedAsRef()
		{
			Assert.DoesNotThrow(() =>
			{
				var array = new NativeArray<UnsafeList<int>>(1, Allocator.Temp);
				array[0] = new UnsafeList<int>(10, Allocator.Temp);
				var item = UnsafeUtility.ArrayElementAsRef<UnsafeList<int>>(array.GetUnsafePtr(), 0);
				item.Add(1);
				item.Dispose();
				array.Dispose();
			});
		}
		
		[Test]
		public static void LengthDoesNotChangeOnArrayAccess()
		{
			var list = new NativeList<UnsafeList<int>>(Allocator.Temp);

			for(int i = 0; i < 10; i++)
			{
				list.Add(new UnsafeList<int>(0, Allocator.Temp, NativeArrayOptions.UninitializedMemory));
				list[i].Add(5);
				list[i].Add(10);
				list[i].Add(15);
			}

			for(int i = 0; i < 10; i++)
			{
				Assert.AreEqual(0, list[i].Length);
				list[i].Dispose();
			}
			
			list.Dispose();
		}
		
		[Test]
		public static void LengthDoesNotChangeWhenCopyIsNotUsedAsRef()
		{
			var list = new NativeList<UnsafeList<int>>(Allocator.Temp);

			for(int i = 0; i < 10; i++)
			{
				list.Add(new UnsafeList<int>(0, Allocator.Temp, NativeArrayOptions.UninitializedMemory));
				var item = list[i];
				item.Add(5);
				item.Add(10);
				item.Add(15);
			}

			for(int i = 0; i < 10; i++)
			{
				Assert.AreEqual(0, list[i].Length);
				list[i].Dispose();
			}
			
			list.Dispose();
		}
		
		[Test]
		public static unsafe void LengthChangesWhenCopyIsUsedAsRef()
		{
			var list = new NativeList<UnsafeList<int>>(Allocator.Temp);

			for(int i = 0; i < 10; i++)
			{
				list.Add(new UnsafeList<int>(0, Allocator.Temp, NativeArrayOptions.UninitializedMemory));
				ref var item = ref UnsafeUtility.ArrayElementAsRef<UnsafeList<int>>(list.GetUnsafePtr(), i);
				item.Add(5);
				item.Add(10);
				item.Add(15);
			}

			for(int i = 0; i < 10; i++)
			{
				Assert.AreEqual(3, list[i].Length);
				list[i].Dispose();
			}
			
			list.Dispose();
		}
	}
}

