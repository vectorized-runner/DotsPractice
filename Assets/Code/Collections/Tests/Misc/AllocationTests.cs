using System;
using NUnit.Framework;
using Unity.Collections.LowLevel.Unsafe;

namespace DotsLibrary.Collections.Tests
{
	public static unsafe class AllocationTests
	{
		[Test]
		public static void CantReadFromZeroPointer()
		{
			Assert.Throws<NullReferenceException>(() =>
			{
				var ptr = (void*)IntPtr.Zero;
				UnsafeUtility.ReadArrayElement<int>(ptr, 0);
			});
		}

		[Test]
		public static void CantReadFromNullPointer()
		{
			Assert.Throws<NullReferenceException>(() =>
			{
				var ptr = (void*)null;
				UnsafeUtility.ReadArrayElement<int>(ptr, 0);
			});
		}
	}
}