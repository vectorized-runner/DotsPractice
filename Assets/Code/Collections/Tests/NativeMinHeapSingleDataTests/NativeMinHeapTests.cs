using System;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace DotsLibrary.Collections.Tests
{
	[BurstCompile]
	struct DeallocateOnCompletionJob : IJob
	{
		public int Value;

		[DeallocateOnJobCompletion]
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			for(int i = 0; i < 10; i++)
			{
				MinHeapSingleData.Push(Value);
			}
		}
	}

	[BurstCompile]
	struct ReadOnlyClearJob : IJob
	{
		[ReadOnly]
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.Clear();
		}
	}

	[BurstCompile]
	struct SimpleJob : IJob
	{
		public NativeReference<int> Min;
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(-1);
			MinHeapSingleData.Push(30);

			Min.Value = MinHeapSingleData.Pop();
		}
	}

	[BurstCompile]
	struct ReadOnlyPushJob : IJob
	{
		[ReadOnly]
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.Push(10);
		}
	}

	[BurstCompile]
	struct WriteOnlyTryPeekJob : IJob
	{
		[WriteOnly]
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.TryPeek(out _);
		}
	}

	[BurstCompile]
	struct ReadOnlyTryPopJob : IJob
	{
		[ReadOnly]
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.TryPop(out _);
		}
	}

	[BurstCompile]
	struct TryPopJob : IJob
	{
		public NativeMinHeapSingleData<int> MinHeapSingleData;
		public NativeReference<int> Min1;
		public NativeReference<int> Min2;
		public NativeReference<int> Min3;

		public void Execute()
		{
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(-10);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.TryPop(out var value1);
			Min1.Value = value1;
			MinHeapSingleData.TryPop(out var value2);
			Min2.Value = value2;
			MinHeapSingleData.TryPop(out var value3);
			Min3.Value = value3;
		}
	}

	[BurstCompile]
	struct PushElementsJob : IJob
	{
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(-1);
			MinHeapSingleData.Push(30);
		}
	}

	[BurstCompile]
	struct ClearJob : IJob
	{
		public NativeMinHeapSingleData<int> MinHeapSingleData;

		public void Execute()
		{
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(-50);
			MinHeapSingleData.Push(0);
			MinHeapSingleData.Clear();
		}
	}

	[BurstCompile]
	struct TryPeekJob : IJob
	{
		public NativeMinHeapSingleData<int> MinHeapSingleData;
		public NativeReference<int> Min1;
		public NativeReference<int> Min2;

		public void Execute()
		{
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(-10);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.TryPeek(out var value1);
			Min1.Value = value1;
			MinHeapSingleData.TryPeek(out var value2);
			Min2.Value = value2;
		}
	}

	public static class NativeMinHeapTests
	{
		static NativeMinHeapSingleData<int> MinHeapSingleData;

		[TearDown]
		public static void TearDown()
		{
			if(MinHeapSingleData.IsCreated)
				MinHeapSingleData.Dispose();
		}

		[Test]
		public static void CapacityIsOneByDefault()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			Assert.AreEqual(1, MinHeapSingleData.Capacity);
		}

		[Test]
		public static void CapacityIsTwoAfterOneItemPush()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(0);
			Assert.AreEqual(2, MinHeapSingleData.Capacity);
		}

		[Test]
		public static void NativeMinHeapImpl_1()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			Assert.AreEqual(10, MinHeapSingleData.Peek());
		}

		[Test]
		public static void NativeMinHeapImpl_2()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(5);
			Assert.AreEqual(5, MinHeapSingleData.Peek());
		}

		[Test]
		public static void NativeMinHeapImpl_3()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(5);
			MinHeapSingleData.Push(10);
			Assert.AreEqual(5, MinHeapSingleData.Peek());
		}

		[Test]
		public static void NativeMinHeapImpl_4()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(-1);
			MinHeapSingleData.Push(1);
			MinHeapSingleData.Push(5);
			Assert.AreEqual(-1, MinHeapSingleData.Peek());
		}

		[Test]
		public static void NativeMinHeapImpl_5()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			Assert.IsTrue(MinHeapSingleData.IsEmpty);
		}

		[Test]
		public static void NativeMinHeapImpl_6()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(1);
			Assert.IsFalse(MinHeapSingleData.IsEmpty);
		}

		[Test]
		public static void NativeMinHeapImpl_7()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			Assert.Throws<IndexOutOfRangeException>(() => Debug.Log(MinHeapSingleData.Peek()));
		}

		[Test]
		public static void NativeMinHeapImpl_8()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(-1);
			MinHeapSingleData.Push(1);
			MinHeapSingleData.Push(5);
			MinHeapSingleData.Push(-213235);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(-200);
			MinHeapSingleData.Push(1342456);
			Assert.AreEqual(-213235, MinHeapSingleData.Peek());
		}

		[Test]
		public static void NativeMinHeapImpl_9()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(5);

			Assert.AreEqual(5, MinHeapSingleData.Pop());
			Assert.AreEqual(10, MinHeapSingleData.Pop());
		}

		[Test]
		public static void NativeMinHeapImpl_10()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			Assert.AreEqual(10, MinHeapSingleData.Pop());
		}

		[Test]
		public static void NativeMinHeapImpl_11()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(-40);
			MinHeapSingleData.Push(1000);
			MinHeapSingleData.Push(5);
			Assert.AreEqual(-40, MinHeapSingleData.Pop());
			Assert.AreEqual(5, MinHeapSingleData.Pop());
			Assert.AreEqual(10, MinHeapSingleData.Pop());
			Assert.AreEqual(1000, MinHeapSingleData.Pop());
			Assert.IsTrue(MinHeapSingleData.IsEmpty);
		}

		[Test]
		public static void NativeMinHeapImpl_12()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(-40);
			MinHeapSingleData.Push(1000);
			MinHeapSingleData.Push(5);
			MinHeapSingleData.Push(int.MaxValue);
			MinHeapSingleData.Push(int.MinValue);
			MinHeapSingleData.Push(123125);
			MinHeapSingleData.Push(-123125);

			Assert.AreEqual(int.MinValue, MinHeapSingleData.Pop());
			Assert.AreEqual(-123125, MinHeapSingleData.Pop());
			Assert.AreEqual(-40, MinHeapSingleData.Pop());
			Assert.AreEqual(5, MinHeapSingleData.Pop());
			Assert.AreEqual(10, MinHeapSingleData.Pop());
			Assert.AreEqual(1000, MinHeapSingleData.Pop());
			Assert.AreEqual(123125, MinHeapSingleData.Pop());
			Assert.AreEqual(int.MaxValue, MinHeapSingleData.Pop());
			Assert.IsTrue(MinHeapSingleData.IsEmpty);
		}

		[Test]
		public static void NativeMinHeapImpl_13()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(-40);
			MinHeapSingleData.Push(1000);
			MinHeapSingleData.Push(5);

			Assert.AreEqual(4, MinHeapSingleData.Length);

			MinHeapSingleData.Pop();
			Assert.AreEqual(3, MinHeapSingleData.Length);

			MinHeapSingleData.Pop();
			Assert.AreEqual(2, MinHeapSingleData.Length);

			MinHeapSingleData.Pop();
			Assert.AreEqual(1, MinHeapSingleData.Length);

			MinHeapSingleData.Pop();
			Assert.AreEqual(0, MinHeapSingleData.Length);
		}

		[Test]
		public static void BuildHeap()
		{
			var list = new NativeList<int>(Allocator.Temp)
			{
				92, 47, 21, 20, 12, 45, 63, 61, 17, 55, 37, 25, 64, 83, 73
			};

			MinHeapSingleData = new NativeMinHeapSingleData<int>(list, Allocator.Temp);

			Assert.AreEqual(12, MinHeapSingleData.Pop());
			Assert.AreEqual(17, MinHeapSingleData.Pop());
			Assert.AreEqual(20, MinHeapSingleData.Pop());
			Assert.AreEqual(21, MinHeapSingleData.Pop());
			Assert.AreEqual(25, MinHeapSingleData.Pop());
			Assert.AreEqual(37, MinHeapSingleData.Pop());
			Assert.AreEqual(45, MinHeapSingleData.Pop());
			Assert.AreEqual(47, MinHeapSingleData.Pop());
			Assert.AreEqual(55, MinHeapSingleData.Pop());
			Assert.AreEqual(61, MinHeapSingleData.Pop());
			Assert.AreEqual(63, MinHeapSingleData.Pop());
			Assert.AreEqual(64, MinHeapSingleData.Pop());
			Assert.AreEqual(73, MinHeapSingleData.Pop());
			Assert.AreEqual(83, MinHeapSingleData.Pop());
			Assert.AreEqual(92, MinHeapSingleData.Pop());
			Assert.IsTrue(MinHeapSingleData.IsEmpty);
		}

		[Test]
		public static void IsNotCreatedByDefault()
		{
			Assert.IsFalse(MinHeapSingleData.IsCreated);
		}

		[Test]
		public static void IsNotCreatedAfterDisposed()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			MinHeapSingleData.Dispose();
			Assert.IsFalse(MinHeapSingleData.IsCreated);
		}

		[Test]
		public static void IsCreatedAfterConstructorCall()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			Assert.IsTrue(MinHeapSingleData.IsCreated);
		}

		[Test]
		public static void ThrowsExceptionOnPopWhenZeroLength()
		{
			Assert.Throws<IndexOutOfRangeException>(() =>
			{
				MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
				MinHeapSingleData.Pop();
			});
		}

		[Test]
		public static void ThrowsExceptionOnPushWhenDeclaredAsReadOnly()
		{
			TestUtil.ExpectInvalidOperationException();

			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			new ReadOnlyPushJob
			{
				MinHeapSingleData = MinHeapSingleData
			}.Run();
		}

		[Test]
		public static void CanBeDisposedAfterAddingSingleElement()
		{
			// If LogAssert.Expect doesn't get called, we're fine
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Dispose();
		}

		[Test]
		public static void CanBeDisposedAfterAddingMultipleElements()
		{
			// If LogAssert.Expect doesn't get called, we're fine
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(-1);
			MinHeapSingleData.Push(30);
		}

		[Test]
		public static void CanBeDisposedAfterAddingElementsInsideJob_HighCapacity()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob, 100);
			var jobHandle = new PushElementsJob
			{
				MinHeapSingleData = MinHeapSingleData,
			}.Schedule();

			jobHandle.Complete();
		}

		[Test]
		public static void CanBeDisposedAfterAddingElementsInsideJob()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			var jobHandle = new PushElementsJob
			{
				MinHeapSingleData = MinHeapSingleData,
			}.Schedule();

			jobHandle.Complete();
		}

		[Test]
		public static void WorksInsideJob()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			var min = new NativeReference<int>(Allocator.TempJob);
			var jobHandle = new SimpleJob
			{
				MinHeapSingleData = MinHeapSingleData,
				Min = min
			}.Schedule();

			jobHandle.Complete();

			Assert.AreEqual(-1, min.Value);

			min.Dispose();
		}

		[Test]
		public static void TryPeekInsideJob()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			var min1 = new NativeReference<int>(Allocator.TempJob);
			var min2 = new NativeReference<int>(Allocator.TempJob);

			new TryPeekJob
			{
				MinHeapSingleData = MinHeapSingleData,
				Min1 = min1,
				Min2 = min2,
			}.Run();

			Assert.AreEqual(-10, min1.Value);
			Assert.AreEqual(-10, min2.Value);

			min1.Dispose();
			min2.Dispose();
		}

		[Test]
		public static void ThrowsExceptionOnPeekWhenDeclaredAsWriteOnly()
		{
			TestUtil.ExpectInvalidOperationException();

			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			new WriteOnlyTryPeekJob { MinHeapSingleData = MinHeapSingleData }.Run();
		}

		[Test]
		public static void TryPopInsideJob()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			var min1 = new NativeReference<int>(Allocator.TempJob);
			var min2 = new NativeReference<int>(Allocator.TempJob);
			var min3 = new NativeReference<int>(Allocator.TempJob);

			new TryPopJob
			{
				MinHeapSingleData = MinHeapSingleData,
				Min1 = min1,
				Min2 = min2,
				Min3 = min3,
			}.Run();

			Assert.AreEqual(-10, min1.Value);
			Assert.AreEqual(10, min2.Value);
			Assert.AreEqual(20, min3.Value);

			min1.Dispose();
			min2.Dispose();
			min3.Dispose();
		}

		[Test]
		public static void ThrowsExceptionOnTryPopWhenDeclaredAsWriteOnly()
		{
			TestUtil.ExpectInvalidOperationException();

			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			new ReadOnlyTryPopJob { MinHeapSingleData = MinHeapSingleData }.Run();
		}

		[Test]
		public static void LengthIsZeroAfterClear()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(200);
			MinHeapSingleData.Push(300);

			Assert.AreNotEqual(0, MinHeapSingleData.Length);

			MinHeapSingleData.Clear();

			Assert.AreEqual(0, MinHeapSingleData.Length);
		}

		[Test]
		public static void TryPopReturnsFalseAfterClear()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(200);
			MinHeapSingleData.Push(300);

			MinHeapSingleData.Clear();

			Assert.IsFalse(MinHeapSingleData.TryPop(out _));
		}

		[Test]
		public static void TryPeekReturnsFalseAfterClear()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(200);
			MinHeapSingleData.Push(300);

			MinHeapSingleData.Clear();

			Assert.IsFalse(MinHeapSingleData.TryPeek(out _));
		}

		[Test]
		public static void PeekThrowsExceptionAfterClear()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(200);
			MinHeapSingleData.Push(300);

			MinHeapSingleData.Clear();

			Assert.Throws<IndexOutOfRangeException>(() => { MinHeapSingleData.Peek(); });
		}

		[Test]
		public static void PopThrowsExceptionAfterClear()
		{
			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.Temp);
			MinHeapSingleData.Push(100);
			MinHeapSingleData.Push(200);
			MinHeapSingleData.Push(300);

			MinHeapSingleData.Clear();

			Assert.Throws<IndexOutOfRangeException>(() => { MinHeapSingleData.Pop(); });
		}

		[Test]
		public static void DoesNotThrowExceptionIfClearedInsideJob()
		{
			Assert.DoesNotThrow(() =>
			{
				MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
				new ClearJob
				{
					MinHeapSingleData = MinHeapSingleData
				}.Run();

				MinHeapSingleData.Dispose();
			});
		}

		[Test]
		public static void ThrowsExceptionOnClearInJobWhenDeclaredAsReadOnly()
		{
			TestUtil.ExpectInvalidOperationException();

			MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);
			MinHeapSingleData.Push(10);
			MinHeapSingleData.Push(20);
			MinHeapSingleData.Push(30);

			new ReadOnlyClearJob
			{
				MinHeapSingleData = MinHeapSingleData
			}.Run();
		}

		[Test]
		public static void DoesNotSupportDeallocateOnCompletion()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				MinHeapSingleData = new NativeMinHeapSingleData<int>(Allocator.TempJob);

				var jobHandle = new DeallocateOnCompletionJob
				{
					MinHeapSingleData = MinHeapSingleData,
					Value = 100,
				}.Schedule();

				jobHandle.Complete();
			});
		}
	}
}