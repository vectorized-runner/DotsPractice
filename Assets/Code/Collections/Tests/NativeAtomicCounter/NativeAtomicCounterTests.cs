using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace NativeCollections.Tests
{
	[BurstCompile]
	struct SetJob : IJob
	{
		[WriteOnly]
		public NativeAtomicCounter Counter;
		public int Value;

		public void Execute()
		{
			Counter.Count = Value;
		}
	}

	[BurstCompile]
	struct IncrementJob : IJob
	{
		public NativeAtomicCounter Counter;
		public int Iteration;

		public void Execute()
		{
			for(int i = 0; i < Iteration; i++)
			{
				Counter.Increment();
			}
		}
	}

	[BurstCompile]
	struct DecrementJob : IJob
	{
		public NativeAtomicCounter Counter;
		public int Iteration;

		public void Execute()
		{
			for(int i = 0; i < Iteration; i++)
			{
				Counter.Decrement();
			}
		}
	}

	[BurstCompile]
	public struct IncrementParallelJob : IJobParallelFor
	{
		public NativeAtomicCounter.ParallelWriter Counter;

		public void Execute(int index)
		{
			Counter.Increment();
		}
	}

	[BurstCompile]
	struct DecrementParallelJob : IJobParallelFor
	{
		public NativeAtomicCounter.ParallelWriter Counter;

		public void Execute(int index)
		{
			Counter.Decrement();
		}
	}

	[BurstCompile]
	struct AddParallelJob : IJobParallelFor
	{
		public NativeAtomicCounter.ParallelWriter Counter;
		public int Amount;

		public void Execute(int index)
		{
			Counter.Add(Amount);
		}
	}

	public static class NativeAtomicCounterTests
	{
		static NativeAtomicCounter Counter;

		[TearDown]
		public static void TearDown()
		{
			Counter.Dispose();
		}

		[Test]
		public static void DefaultValueIsZero()
		{
			Counter = new NativeAtomicCounter(Allocator.Temp);
			Assert.AreEqual(0, Counter.Count);
		}

		[Test]
		public static void CanSetInitialValueInConstructor()
		{
			Counter = new NativeAtomicCounter(Allocator.Temp, 10);
			Assert.AreEqual(10, Counter.Count);
		}

		[Test]
		public static void CanSetValue()
		{
			Counter = new NativeAtomicCounter(Allocator.Temp, 10);
			Counter.Count = 20;

			Assert.AreEqual(20, Counter.Count);
		}

		[Test]
		public static void CanIncrementValue()
		{
			Counter = new NativeAtomicCounter(Allocator.Temp, 10);
			Counter.Increment();

			Assert.AreEqual(11, Counter.Count);
		}

		[Test]
		public static void CanDecrementValue()
		{
			Counter = new NativeAtomicCounter(Allocator.Temp, 10);
			Counter.Decrement();

			Assert.AreEqual(9, Counter.Count);
		}

		[Test]
		public static void CanIncrementInJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new IncrementJob
			{
				Counter = Counter,
				Iteration = 100
			}.Schedule();
			jobHandle.Complete();

			Assert.AreEqual(110, Counter.Count);
		}

		[Test]
		public static void CanDecrementInJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new DecrementJob
			{
				Counter = Counter,
				Iteration = 100
			}.Schedule();
			jobHandle.Complete();

			Assert.AreEqual(-90, Counter.Count);
		}

		[Test]
		public static void CanSetInJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new SetJob
			{
				Counter = Counter,
				Value = 100
			}.Schedule();
			jobHandle.Complete();

			Assert.AreEqual(100, Counter.Count);
		}

		[Test]
		public static void CanIncrementInParallelJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new IncrementParallelJob
			{
				Counter = Counter.AsParallelWriter(),
			}.Schedule(100, 1);
			jobHandle.Complete();

			Assert.AreEqual(110, Counter.Count);
		}

		[Test]
		public static void CanAddInParallelJob_HighBatchCount()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob);
			var jobHandle = new AddParallelJob
			{
				Counter = Counter.AsParallelWriter(),
				Amount = 10,
			}.Schedule(6400, 64);
			jobHandle.Complete();

			Assert.AreEqual(64000, Counter.Count);
		}

		[Test]
		public static void CanIncrementInParallelJob_HighBatchCount()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob);
			var jobHandle = new IncrementParallelJob
			{
				Counter = Counter.AsParallelWriter(),
			}.Schedule(6400, 64);
			jobHandle.Complete();

			Assert.AreEqual(6400, Counter.Count);
		}

		[Test]
		public static void CanDecrementInParallelJob_HighBatchCount()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob);
			var jobHandle = new DecrementParallelJob
			{
				Counter = Counter.AsParallelWriter(),
			}.Schedule(6400, 64);
			jobHandle.Complete();

			Assert.AreEqual(-6400, Counter.Count);
		}

		[Test]
		public static void CanDecrementInParallelJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new DecrementParallelJob
			{
				Counter = Counter.AsParallelWriter(),
			}.Schedule(100, 1);
			jobHandle.Complete();

			Assert.AreEqual(-90, Counter.Count);
		}

		[Test]
		public static void CanAddInParallelJob()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob, 10);
			var jobHandle = new AddParallelJob
			{
				Counter = Counter.AsParallelWriter(),
				Amount = 10,
			}.Schedule(100, 1);
			jobHandle.Complete();

			Assert.AreEqual(1010, Counter.Count);
		}

		[Test]
		public static void ParallelIncrementThenNonParallelIncrement()
		{
			Counter = new NativeAtomicCounter(Allocator.TempJob);
			var parallelWriter = Counter.AsParallelWriter();
			var job1 = new IncrementParallelJob
			{
				Counter = parallelWriter
			}.Schedule(10000, 10);
			var job2 = new IncrementJob
			{
				Counter = Counter,
				Iteration = 10000
			}.Schedule(job1);

			JobHandle.CompleteAll(ref job1, ref job2);

			Assert.AreEqual(20000, Counter.Count);
		}
		
		// Can't pass this because TearDown dispose also throws InvalidOperationException, but it works.
		// [Test]
		// public static void ThrowsInvalidOperationExceptionOnScheduleWriteWithoutDependencyChain()
		// {
		// 	Assert.Throws<InvalidOperationException>(() =>
		// 	{
		// 		Counter = new NativeCounter(Allocator.TempJob);
		// 		var job1 = new IncrementJob
		// 		{
		// 			IntPtr = IntPtr,
		// 			Iteration = 10000
		// 		}.Schedule();
		// 		var job2 = new IncrementJob
		// 		{
		// 			IntPtr = IntPtr,
		// 			Iteration = 10000
		// 		}.Schedule();
		//
		// 		JobHandle.CompleteAll(ref job1, ref job2);
		// 	});
		// }
	}
}