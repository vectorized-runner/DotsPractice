using System.Threading;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.PerformanceTesting;

namespace NativeCollections.Tests
{
	[BurstCompile]
	public static unsafe class NativeAtomicCounterBenchmarks
	{
		const int IncrementCount = 10_000_000;

		[BurstCompile]
		struct InterlockedIncrementJob : IJobParallelFor
		{
			[NativeDisableUnsafePtrRestriction]
			public int* Counter;

			public void Execute(int index)
			{
				Interlocked.Increment(ref *Counter);
			}
		}

		[Test, Performance]
		public static void IncrementInterlockedDefaultInt()
		{
			int* count = null;

			Measure.Method(() =>
			       {
				       var jobHandle = new InterlockedIncrementJob
				       {
					       Counter = count
				       }.Schedule(IncrementCount, 64);

				       jobHandle.Complete();
			       })
			       .SetUp(() =>
			       {
				       count = (int*)UnsafeUtility.Malloc(
					       UnsafeUtility.SizeOf<int>(),
					       UnsafeUtility.AlignOf<int>(),
					       Allocator.TempJob);
			       })
			       .CleanUp(() => { UnsafeUtility.Free(count, Allocator.TempJob); })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}

		[Test, Performance]
		public static void IncrementMultiThreadedCounter()
		{
			NativeAtomicCounter counter = default;

			Measure.Method(() =>
			       {
				       var jobHandle = new IncrementParallelJob
				       {
					       Counter = counter.AsParallelWriter(),
				       }.Schedule(IncrementCount, 64);

				       jobHandle.Complete();
			       })
			       .SetUp(() => { counter = new NativeAtomicCounter(Allocator.TempJob); })
			       .CleanUp(() => { counter.Dispose(); })
			       .WarmupCount(10)
			       .IterationsPerMeasurement(10)
			       .MeasurementCount(10)
			       .Run();
		}
	}
}