using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Burst.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;
// ReSharper disable MemberCanBePrivate.Global

namespace NativeCollections
{
	[StructLayout(LayoutKind.Sequential)]
	[NativeContainer]
	public unsafe struct NativeMinHeap<T, TComparer> : INativeDisposable where T : unmanaged where TComparer : unmanaged, IComparer<T>
	{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
		internal AtomicSafetyHandle m_Safety;

		static readonly SharedStatic<int> s_staticSafetyId = SharedStatic<int>.GetOrCreate<NativeMinHeap<T, TComparer>>();

		[BurstDiscard]
		static void InitStaticSafetyId(ref AtomicSafetyHandle safetyHandle)
		{
			if(s_staticSafetyId.Data == 0)
				s_staticSafetyId.Data = AtomicSafetyHandle.NewStaticSafetyId<NativeList<T>>();
			
			AtomicSafetyHandle.SetStaticSafetyId(ref safetyHandle, s_staticSafetyId.Data);
		}

		[NativeSetClassTypeToNullOnSchedule]
		DisposeSentinel m_DisposeSentinel;
#endif

		[NativeDisableUnsafePtrRestriction]
		internal UnsafeList<T>* Data;

		internal Allocator Allocator;

		TComparer Comparer;

		public NativeMinHeap(NativeArray<T> array, TComparer comparer, Allocator allocator)
		{
			var length = array.Length;
			if(length == 0)
				throw new ArgumentOutOfRangeException($"{nameof(NativeArray<T>)} length must be greater than zero: {array.Length}");

			var capacity = math.ceilpow2(length + 1);
			Allocate(capacity, comparer, allocator, out this);
			AddRange(array);
		}
		
		public NativeMinHeap(NativeList<T> list, TComparer comparer, Allocator allocator)
		{
			var length = list.Length;
			if(length == 0)
				throw new ArgumentOutOfRangeException($"{nameof(NativeArray<T>)} length must be greater than zero: {list.Length}");

			var capacity = math.ceilpow2(length + 1);
			Allocate(capacity, comparer, allocator, out this);
			AddRange(list);
		}

		public NativeMinHeap(int capacity, TComparer comparer, Allocator allocator)
		{
			Allocate(capacity, comparer, allocator, out this);
		}
		
		public NativeMinHeap(TComparer comparer, Allocator allocator)
		{
			Allocate(1, comparer, allocator, out this);
		}

		static void Allocate(int capacity, TComparer comparer, Allocator allocator, out NativeMinHeap<T, TComparer> minHeap)
		{
			var totalSize = UnsafeUtility.SizeOf<T>() * (long)capacity;
			minHeap = new NativeMinHeap<T, TComparer>();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckAllocateArguments(capacity, allocator, totalSize);
			DisposeSentinel.Create(out minHeap.m_Safety, out minHeap.m_DisposeSentinel, 1, allocator);
			InitStaticSafetyId(ref minHeap.m_Safety);
#endif
			minHeap.Data = UnsafeList<T>.Create(capacity, allocator);
			minHeap.Allocator = allocator;
			minHeap.Comparer = comparer;
			
			// TODO: Do I need AtomicSafetyHandle.SetBumpSecondaryVersionOnScheduleWrite here, as NativeList does?
		}

		public bool IsCreated => Data != null;
		
		public int Capacity
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
				return AssumePositive(Data->Capacity);
			}
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
				CheckCapacityInRange(value, Data->Length);
				Data->SetCapacity(value);
			}
		}

		public int Count
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
				return AssumePositive(Data->Length);
			}
			private set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
				Data->Length = value;
			}
		}

		// Same as CollectionHelper.AssumePositive, but can't be used since it's internal
		[return: AssumeRange(0, int.MaxValue)]
		static int AssumePositive(int value)
		{
			return value;
		}

		public bool IsEmpty => !IsCreated || Count == 0;

		T this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
				CheckIndexInRange(index, Capacity);
				return UnsafeUtility.ReadArrayElement<T>(Data->Ptr, AssumePositive(index));
			}
			[WriteAccessRequired]
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				AtomicSafetyHandle.CheckWriteAndThrow(m_Safety);				
#endif
				CheckIndexInRange(index, Capacity);
				UnsafeUtility.WriteArrayElement(Data->Ptr, AssumePositive(index), value);
			}
		}

		public bool TryPeek(out T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
			if(Count > 0)
			{
				item = Peek();
				return true;
			}

			item = default;
			return false;
		}

		[WriteAccessRequired]
		public bool TryRemove(out T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
			if(Count > 0)
			{
				item = Remove();
				return true;
			}

			item = default;
			return false;
		}

		[WriteAccessRequired]
		public T Remove()
		{
			CheckIsEmptyAndThrow();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
			var item = this[1];
			this[1] = this[Count--];
			BubbleDown(1);

			return item;
		}

		public T Peek()
		{
			CheckIsEmptyAndThrow();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
#endif
			return this[1];
		}

		[WriteAccessRequired]
		public void AddRange(NativeList<T> list)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
			var requiredCapacity = math.ceilpow2(Count + 1);
			if(requiredCapacity > Capacity)
			{
				Capacity = requiredCapacity;
			}
			
			AddRangeNoCheck(list.GetUnsafeReadOnlyPtr(), list.Length);
		}

		[WriteAccessRequired]
		public void AddRange(NativeArray<T> array)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
			var requiredCapacity = math.ceilpow2(Count + 1);
			if(requiredCapacity > Capacity)
			{
				Capacity = requiredCapacity;
			}
			
			AddRangeNoCheck(array.GetUnsafeReadOnlyPtr(), array.Length);
		}

		void AddRangeNoCheck(void* ptr, int count)
		{
			var elementSize = UnsafeUtility.SizeOf<T>();
			var offset = (Count + 1) * elementSize;
			var byteCount = count * elementSize;
            UnsafeUtility.MemCpy((byte*)Data->Ptr + offset, ptr, byteCount);
            Data->Length += count;
            BuildHeap();
		}

		[WriteAccessRequired]
		public void Add(T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
#endif
			var requiredCapacity = math.ceilpow2(Count + 1);
			if(requiredCapacity > Capacity)
			{
				Capacity = requiredCapacity;
			}

			this[++Count] = item;
			this[0] = item;
			var holeIndex = Count;
			BubbleUp(item, holeIndex);
		}

		[WriteAccessRequired]
		public void Clear()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);			
#endif
			Data->Clear();
		}

		public void Dispose()
		{
			CheckIsInvalidAllocatorAndThrow();
			CheckIsDisposedAndThrow();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
			UnsafeList<T>.Destroy(Data);
			Data = null;
			Allocator = Allocator.Invalid;
		}

		public JobHandle Dispose(JobHandle inputDeps)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Clear(ref m_DisposeSentinel);

			var jobHandle = new NativeHeapDisposeJob
			{
				Data = new NativeHeapDispose { Data = Data },
			}.Schedule(inputDeps);

			AtomicSafetyHandle.Release(m_Safety);
#else
			var jobHandle = new NativeMinHeapDisposeJob
			{
				Data = new NativeMinHeapDispose { Data = Data },
			}.Schedule(inputDeps);
#endif
			return jobHandle;
		}
		
		void BuildHeap()
		{
			for(int i = Count / 2; i > 0; i--)
				BubbleDown(i);
		}
		
		void BubbleDown(int holeIndex)
		{
			var temp = this[holeIndex];

			while(GetLeftChildIndex(holeIndex) <= Count)
			{
				var childIndex = GetLeftChildIndex(holeIndex);

				// If right child is smaller, use right child index
				if(
					childIndex != Count &&
					Comparer.Compare(this[childIndex + 1], this[childIndex]) < 0)
				{
					childIndex++;
				}

				if(Comparer.Compare(this[childIndex], temp) < 0)
				{
					this[holeIndex] = this[childIndex];
				}
				else
				{
					break;
				}

				holeIndex = childIndex;
			}

			this[holeIndex] = temp;
		}
		
		void BubbleUp(T item, int holeIndex)
		{
			while(Comparer.Compare(item, this[GetParentIndex(holeIndex)]) < 0)
			{
				this[holeIndex] = this[GetParentIndex(holeIndex)];
				holeIndex /= 2;
			}

			this[holeIndex] = item;
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		int GetLeftChildIndex(int index)
		{
			return 2 * index;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		int GetParentIndex(int index)
		{
			return index / 2;
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocateArguments(int capacity, Allocator allocator, long totalSize)
		{
			CheckAllocator(allocator);
			CheckCapacity(capacity);
			CheckTotalSize(nameof(capacity), totalSize);
			CheckIsBlittable();
			CheckIsValidElementType();
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsValidElementType()
		{
			if(!UnsafeUtility.IsValidNativeContainerElementType<T>())
				throw new InvalidOperationException(
					$"{typeof(T)} used in NativeMinHeap<{typeof(T)}> must be unmanaged (contain no managed types) and cannot itself be a native container type.");
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsBlittable()
		{
			if(!UnsafeUtility.IsBlittable<T>())
				throw new ArgumentException(string.Format("{0} used in NativeMinHeap<{0}> must be blittable",
					typeof(T)));
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckTotalSize(string paramName, long totalSize)
		{
			// Make sure we cannot allocate more than int.MaxValue (2,147,483,647 bytes)
			// because the underlying UnsafeUtility.Malloc is expecting a int.
			if(totalSize > int.MaxValue)
				throw new ArgumentOutOfRangeException(paramName,
					$"Length * sizeof(T) cannot exceed {int.MaxValue} bytes");
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocator(Allocator allocator)
		{
			// Native allocation is only valid for Temp, Job and Persistent.
			if(allocator <= Allocator.None)
				throw new ArgumentException("Allocator must be Temp, TempJob or Persistent", nameof(allocator));
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckCapacity(int capacity)
		{
			if(capacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "Capacity must be greater than zero.");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIndexInRange(int value, int length)
		{
			if(value < 0)
				throw new IndexOutOfRangeException($"Value {value} must be positive.");

			if((uint)value >= (uint)length)
				throw new IndexOutOfRangeException(
					$"Value {value} is out of range in {nameof(NativeMinHeap<T, TComparer>)} of '{length}' Length.");
		}
		
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckCapacityInRange(int value, int length)
		{
			if (value < 0)
				throw new ArgumentOutOfRangeException($"Value {value} must be positive.");

			if ((uint)value < (uint)length)
				throw new ArgumentOutOfRangeException($"Value {value} is out of range in NativeList of '{length}' Length.");
		}
		
		void CheckIsEmptyAndThrow()
		{
			if(IsEmpty)
				throw new IndexOutOfRangeException($"{nameof(NativeMinHeap<T, TComparer>)} is empty, but you're trying to read from it.");
		}
		
		void CheckIsDisposedAndThrow()
		{
			if(!IsCreated)
				throw new ObjectDisposedException($"The {nameof(NativeMinHeap<T, TComparer>)} is already disposed.");
		}

		void CheckIsInvalidAllocatorAndThrow()
		{
			if(Allocator == Allocator.Invalid)
				throw new InvalidOperationException($"The {nameof(NativeMinHeap<T, TComparer>)} can not be Disposed because it was not allocated with a valid allocator.");
		}

		struct NativeHeapDispose
		{
			[NativeDisableUnsafePtrRestriction]
			public UnsafeList<T>* Data;
    
			public void Dispose()
			{
				UnsafeList<T>.Destroy(Data);
			}
		}
		
		[BurstCompile]
		struct NativeHeapDisposeJob : IJob
		{
			public NativeHeapDispose Data;
    
			public void Execute()
			{
				Data.Dispose();
			}
		}
	}
}