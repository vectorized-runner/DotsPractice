using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

// ReSharper disable UseObjectOrCollectionInitializer
// ReSharper disable FieldCanBeMadeReadOnly.Global
// ReSharper disable MemberCanBePrivate.Global
// Important: We keep variables internal and use "m_" naming convention because the Job System needs it. Don't change it on any of existing variables.
namespace DotsLibrary.Collections
{
	[BurstCompile]
	unsafe struct NativeMinHeapDisposeJob : IJob
	{
		public NativeMinHeapDispose Data;

		public void Execute()
		{
			Data.Dispose();
		}
	}

	unsafe struct NativeMinHeapDispose
	{
		[NativeDisableUnsafePtrRestriction]
		public HeapData* Data;

		public void Dispose()
		{
			HeapData.Destroy(Data);
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	unsafe struct HeapData
	{
		[NativeDisableUnsafePtrRestriction]
		internal void* Buffer;
		internal int Length;
		internal int Capacity;
		internal int ElementSize;
		internal int Alignment;
		internal Allocator Allocator;

		public static HeapData* Create(int capacity, int elementSize, int alignment, Allocator allocator)
		{
			var heapData = AllocateHeap(allocator);
			heapData->Length = 0;
			heapData->Capacity = capacity;
			heapData->ElementSize = elementSize;
			heapData->Allocator = allocator;
			heapData->Alignment = alignment;
			heapData->Buffer = heapData->AllocateBuffer(capacity);
			return heapData;
		}

		static HeapData* AllocateHeap(Allocator allocator)
		{
			return (HeapData*)UnsafeUtility.Malloc(UnsafeUtility.SizeOf<HeapData>(), UnsafeUtility.AlignOf<HeapData>(),
				allocator);
		}

		public static void Destroy(HeapData* heapData)
		{
			var allocator = heapData->Allocator;
			UnsafeUtility.Free(heapData->Buffer, allocator);
			heapData->Buffer = null;
			heapData->Length = 0;
			UnsafeUtility.Free(heapData, allocator);
		}

		public void Expand(int newCapacity)
		{
			if(newCapacity <= Capacity)
				throw new ArgumentOutOfRangeException(
					$"New Capacity: {newCapacity} should be greater than current capacity: {Capacity}");

			var newBuffer = AllocateBuffer(newCapacity);
			UnsafeUtility.MemCpy(newBuffer, Buffer, ElementSize * Capacity);
			UnsafeUtility.Free(Buffer, Allocator);
			Buffer = newBuffer;
			Capacity = newCapacity;
		}

		void* AllocateBuffer(int capacity)
		{
			return AllocateBuffer(ElementSize, capacity, Alignment, Allocator);
		}

		static void* AllocateBuffer(int elementSize, int length, int alignment, Allocator allocator)
		{
			return UnsafeUtility.Malloc(elementSize * length, alignment, allocator);
		}
	}

	// TODO: This implementation has still some missing bits.
	// Length and Capacity reads should call AtomicSafetyHandle and
	// Could have WriteAccessRequired on more methods
	[StructLayout(LayoutKind.Sequential)]
	[NativeContainer]
	public unsafe struct NativeMinHeapSingleData<T> : INativeDisposable where T : struct, IComparable<T>
	{
		// For expanding inside jobs, we need to change where the buffer points to,
		// so we use pointer to pointer. Otherwise original copy will be unchanged since
		// structs are passed by value.
		[NativeDisableUnsafePtrRestriction]
		internal HeapData* Data;

		public void* Buffer
		{
			get => Data->Buffer;
			set => Data->Buffer = value;
		}
		public int Length
		{
			get => Data->Length;
			set => Data->Length = value;
		}
		public int Capacity
		{
			get => Data->Capacity;
			set => Data->Capacity = value;
		}

		public bool IsCreated => !IsNullPtr(Data);
		public bool IsEmpty => Length == 0;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
		internal AtomicSafetyHandle m_Safety;

		[NativeSetClassTypeToNullOnSchedule]
		internal DisposeSentinel m_DisposeSentinel;

		static readonly SharedStatic<int> s_staticSafetyId = SharedStatic<int>.GetOrCreate<NativeMinHeapSingleData<T>>();
#endif

		internal Allocator m_AllocatorLabel;

		static bool IsNullPtr(void* ptr)
		{
			return (IntPtr)ptr == IntPtr.Zero;
		}

		public NativeMinHeapSingleData(NativeArray<T> array, Allocator allocator)
		{
			var length = array.Length;

			if(length == 0)
			{
				throw new ArgumentOutOfRangeException($"Array length must be greater than zero: {array.Length}");
			}

			var capacity = math.ceilpow2(length + 1);
			Allocate(capacity, allocator, out this);
			var readonlyPtr = array.GetUnsafeReadOnlyPtr();
			var elementSize = UnsafeUtility.SizeOf<T>();
			var offset = elementSize;
			// Notice you have to cast to byte*, int* doesn't yield the same result.
			// Same as the commented out code below, just faster.
			UnsafeUtility.MemCpy((byte*)Buffer + offset, readonlyPtr, elementSize * length);
			// for(int i = 1; i < length + 1; i++)
			// {
			// 	UnsafeUtility.WriteArrayElement(m_Buffer, i, UnsafeUtility.ReadArrayElement<T>(readonlyPtr, i - 1));
			// }

			Length = length;
			BuildHeap();
		}

		public NativeMinHeapSingleData(Allocator allocator, int initialCapacity = 1)
		{
			Allocate(initialCapacity, allocator, out this);
		}

		T this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckReadAccess();
#endif
				CheckIndexInRange(index, Capacity);
				return UnsafeUtility.ReadArrayElement<T>(Buffer, index);
			}
			[WriteAccessRequired]
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				CheckWriteAccess();
#endif
				CheckIndexInRange(index, Capacity);
				UnsafeUtility.WriteArrayElement(Buffer, index, value);
			}
		}

		public T Peek()
		{
			CheckIsEmptyAndThrow();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckReadAccess();
#endif
			return this[1];
		}

		[WriteAccessRequired]
		public void Clear()
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAndBumpSecondaryVersion();
#endif
			Length = 0;
		}

		[WriteAccessRequired]
		public void Push(T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAndBumpSecondaryVersion();
#endif

			if(Length + 1 >= Capacity)
			{
				Data->Expand(2 * Capacity);
			}

			this[++Length] = item;
			this[0] = item;
			var holeIndex = Length;
			BubbleUp(item, holeIndex);
		}

		void BubbleUp(T item, int holeIndex)
		{
			while(item.CompareTo(this[GetParentIndex(holeIndex)]) < 0)
			{
				this[holeIndex] = this[GetParentIndex(holeIndex)];
				holeIndex /= 2;
			}

			this[holeIndex] = item;
		}

		// Establish heap-order property from an arbitrary arrangement of items. Runs in linear time.
		void BuildHeap()
		{
			for(int i = Length / 2; i > 0; i--)
				BubbleDown(i);
		}

		int GetParentIndex(int index)
		{
			return index / 2;
		}

		int GetLeftChildIndex(int index)
		{
			return 2 * index;
		}

		[WriteAccessRequired]
		public T Pop()
		{
			CheckIsEmptyAndThrow();
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAndBumpSecondaryVersion();
#endif
			var item = this[1];
			this[1] = this[Length--];
			BubbleDown(1);

			return item;
		}

		void BubbleDown(int holeIndex)
		{
			var temp = this[holeIndex];

			while(GetLeftChildIndex(holeIndex) <= Length)
			{
				var childIndex = GetLeftChildIndex(holeIndex);

				// If right child is smaller, use right child index
				if(
					childIndex != Length &&
					this[childIndex + 1].CompareTo(this[childIndex]) < 0)
				{
					childIndex++;
				}

				if(this[childIndex].CompareTo(temp) < 0)
				{
					this[holeIndex] = this[childIndex];
				}
				else
				{
					break;
				}

				holeIndex = childIndex;
			}

			this[holeIndex] = temp;
		}

		public bool TryPeek(out T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckReadAccess();
#endif
			if(Length > 0)
			{
				item = Peek();
				return true;
			}

			item = default;
			return false;
		}

		[WriteAccessRequired]
		public bool TryPop(out T item)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckWriteAndBumpSecondaryVersion();
#endif
			if(Length > 0)
			{
				item = Pop();
				return true;
			}

			item = default;
			return false;
		}

		public void Dispose()
		{
			CheckIsNullPtrAndThrow();
			CheckInvalidAllocatorAndThrow();

			if(m_AllocatorLabel > Allocator.None)
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
				HeapData.Destroy(Data);
				m_AllocatorLabel = Allocator.Invalid;
			}

			Data = null;
		}

		public JobHandle Dispose(JobHandle inputDeps)
		{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Clear(ref m_DisposeSentinel);

			var jobHandle = new NativeMinHeapDisposeJob
			{
				Data = new NativeMinHeapDispose { Data = Data },
			}.Schedule(inputDeps);

			AtomicSafetyHandle.Release(m_Safety);
#else
			var jobHandle = new NativeMinHeapDisposeJob
			{
				Data = new NativeMinHeapDispose { Data = Data },
			}.Schedule(inputDeps);
#endif
			return jobHandle;
		}

		static void Allocate(int capacity, Allocator allocator, out NativeMinHeapSingleData<T> minHeapSingleData)
		{
			var elementSize = UnsafeUtility.SizeOf<T>();
			var alignment = UnsafeUtility.AlignOf<T>();
			var totalSize = elementSize * (long)capacity;
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			CheckAllocateArguments(capacity, allocator, totalSize);
#endif
			minHeapSingleData = new NativeMinHeapSingleData<T>();
			minHeapSingleData.m_AllocatorLabel = allocator;
			minHeapSingleData.Data = HeapData.Create(capacity, elementSize, alignment, allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
			DisposeSentinel.Create(out minHeapSingleData.m_Safety, out minHeapSingleData.m_DisposeSentinel, 1, allocator);
			InitStaticSafetyId(ref minHeapSingleData.m_Safety);
#endif
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocateArguments(int capacity, Allocator allocator, long totalSize)
		{
			CheckAllocator(allocator);
			CheckCapacity(capacity);
			CheckTotalSize(nameof(capacity), totalSize);
			CheckIsBlittable();
			CheckIsValidElementType();
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckAllocator(Allocator allocator)
		{
			// Native allocation is only valid for Temp, Job and Persistent.
			if(allocator <= Allocator.None)
				throw new ArgumentException("Allocator must be Temp, TempJob or Persistent", nameof(allocator));
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckCapacity(int capacity)
		{
			if(capacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "Capacity must be greater than zero.");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckTotalSize(string paramName, long totalSize)
		{
			// Make sure we cannot allocate more than int.MaxValue (2,147,483,647 bytes)
			// because the underlying UnsafeUtility.Malloc is expecting a int.
			if(totalSize > int.MaxValue)
				throw new ArgumentOutOfRangeException(paramName,
					$"Length * sizeof(T) cannot exceed {int.MaxValue} bytes");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsBlittable()
		{
			if(!UnsafeUtility.IsBlittable<T>())
				throw new ArgumentException(string.Format("{0} used in NativeMinHeap<{0}> must be blittable",
					typeof(T)));
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIsValidElementType()
		{
			if(!UnsafeUtility.IsValidNativeContainerElementType<T>())
				throw new InvalidOperationException(
					$"{typeof(T)} used in NativeMinHeap<{typeof(T)}> must be unmanaged (contain no managed types) and cannot itself be a native container type.");
		}

		[BurstDiscard]
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void InitStaticSafetyId(ref AtomicSafetyHandle safetyHandle)
		{
			if(s_staticSafetyId.Data == 0)
				s_staticSafetyId.Data = AtomicSafetyHandle.NewStaticSafetyId<NativeMinHeapSingleData<T>>();

			AtomicSafetyHandle.SetStaticSafetyId(ref safetyHandle, s_staticSafetyId.Data);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckWriteAndBumpSecondaryVersion()
		{
			AtomicSafetyHandle.CheckWriteAndBumpSecondaryVersion(m_Safety);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckWriteAccess()
		{
			AtomicSafetyHandle.CheckWriteAndThrow(m_Safety);
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		void CheckReadAccess()
		{
			AtomicSafetyHandle.CheckReadAndThrow(m_Safety);
		}

		void CheckIsNullPtrAndThrow()
		{
			if(IsNullPtr(Buffer))
				throw new ObjectDisposedException("The NativeMinHeap is already disposed.");
		}

		void CheckInvalidAllocatorAndThrow()
		{
			if(m_AllocatorLabel == Allocator.Invalid)
				throw new InvalidOperationException(
					"The NativeMinHeap can not be Disposed because it was not allocated with a valid allocator.");
		}

		void CheckIsEmptyAndThrow()
		{
			if(IsEmpty)
				throw new IndexOutOfRangeException("NativeMinHeap is empty.");
		}

		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		static void CheckIndexInRange(int index, int length)
		{
			if(index < 0)
				throw new IndexOutOfRangeException($"Value {index} must be positive.");

			if((uint)index >= (uint)length)
				throw new IndexOutOfRangeException(
					$"Value {index} is out of range in NativeList of '{length}' Length.");
		}
	}
}