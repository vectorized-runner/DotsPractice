using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using static Unity.Burst.CompilerServices.Loop;

namespace DotsLibrary.Intrinsics
{
	public static unsafe class ExpectVectorizedIntrinsicExample
	{
		// Does work on pointers, this is doesn't give error
		[BurstCompile]
		struct VectorizedPtrJob : IJob
		{
			[NoAlias]
			public int* Ptr1;
			[NoAlias]
			public int* Ptr2;
			public int Count;

			public void Execute()
			{
				for(int i = 0; i < Count; i++)
				{
					ExpectVectorized();
					Ptr1[i] += Ptr2[i];
				}
			}
		}

		// Does work on this too, doesn't give error
		[BurstCompile]
		struct NotVectorizedPtrJob : IJob
		{
			public int* Ptr1;
			public int* Ptr2;
			public int Count;

			public void Execute()
			{
				for(int i = 0; i < Count; i++)
				{
					ExpectNotVectorized();

					if(Ptr1[i] > Ptr2[i])
						break;

					Ptr1[i] += Ptr2[i];
				}
			}
		}

		// This is not vectorized I guess, since it gives error if you remove the comment
		[BurstCompile]
		struct VectorizedArrayJob : IJob
		{
			[NoAlias]
			public NativeArray<int> Array1;
			[NoAlias]
			public NativeArray<int> Array2;
			
			public void Execute()
			{
				for(int i = 0; i < Array1.Length; i++)
				{
					// ExpectVectorized();
					Array1[i] += Array2[i];
				}
			}
		}

		// Does not give error
		[BurstCompile]
		struct NotVectorizedArrayJob : IJob
		{
			[NoAlias]
			public NativeArray<int> Array1;
			[NoAlias]
			public NativeArray<int> Array2;

			public void Execute()
			{
				for(int i = 0; i < Array1.Length; i++)
				{
					ExpectNotVectorized();
					if(Array1[i] > Array2[i])
					{
						Array1[i] += Array2[i];
					}
				}
			}
		}
	}
}